# Thesis M2 - Isis Marques

This web application was made to gather data for the master thesis of Isis Marques in M2 level in Economics & Psychology Master. Master in partenership between the [Psychological Institute at University Paris Descartes](http://ecolesdoctorales.parisdescartes.fr/ed261/Les-laboratoires/Equipes-de-recherche), the [Centre d'Economie de la Sorbonne at University Paris 1](http://centredeconomiesorbonne.univ-paris1.fr/) and the [Paris School of Economics](www.parisschoolofeconomics.eu/en/academic-staff/associate-members/).

## Goal

Build an application where humans can interact as either **Buyers** or **Sellers** in a **Game** of `N` **rounds**.

**Sellers** can choose to buy a level of certification granting them a certain "notoriety" at the beginning of the game.
In each round they set _prices_ and _quality_ on different items of the same product.

> The goal of the seller is to maximise its _profit_.

![](./img/seller_first_round.png)

**Buyers** choose to buy the _items_ set by the **sellers**, without knowing the quality of the product but knowing only the **sellers' certification level**.
At each round a total **utility** level is calculated (for now the utility function is **not** configurable) and shown to the buyer.

At each round, **buyers** have the detail of satisfaction derived for **each** item bought.

> The goal of the buyer is to maximise their utility and spend the least of their money.

![](./img/buyers_view.png)

## Settings

Most of the values used in the game are configurable via the administration page `/admin`.

The full list of configurable settings:

| Key                           | Description                                                                                                                                                                                                       | Default Value |
|-------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------|
| buyer_initial_money           | Initial money available to buyers                                                                                                                                                                                 | 2000          |
| seller_initial_money          | Initial money available to sellers                                                                                                                                                                                | 2000          |
| seller_certificate_min        | Minimum value for sellers' certificate                                                                                                                                                                            | 1             |
| seller_certificate_max        | Maximum value for sellers' certificate                                                                                                                                                                            | 3             |
| seller_certificate_cost_coef  | Coefficient used in cost function of the certificate. Cost function can be [found here](https://gitlab.com/isismqs/citron-market/blob/cb11cd427bb5109102cb5fae33805ce1c7a70b71/app/helpers/utils_helper.rb#L2-9). | 40            |
| seller_certificate_rounds     | Fraction the payment of sellers' certificate into n rounds                                                                                                                                                        | 4             |
| number_products_per_seller    | Amount of items sold per seller during each round                                                                                                                                                                 | 3             |
| number_products_per_buyer     | Maximum amount of items buyers can purchase                                                                                                                                                                       | 3             |
| number_products_per_buyer_min | Minimum amount of items buyers can purchase >0. **`0` WILL NOT WORK (yet) if you need a setup with 0 please consider contributing or asking via an issue**                                                        | 3             |
| product_quality_min           | Minimum quality per item sold                                                                                                                                                                                     | 1             |
| product_quality_max           | Maximum quality per item sold                                                                                                                                                                                     | 4             |
| product_price_min             | Minimum price per item sold                                                                                                                                                                                       | 1             |
| product_price_max             | Maximum price per item sold                                                                                                                                                                                       | 50            |
| rounds_start_audit            | Round number when sellers' will be audited for their items' quality (Set this value higher than `rounds_count` if you don't want any audits)                                                                      | 6             |
| rounds_public_audit           | Round number when buyers will be _aware_ of the sellers' audits. (Set this value higher than `rounds_count` if you don't want any audits)                                                                         | 11            |
| rounds_first_duration         | Time available for players to finish the first round                                                                                                                                                              | 120           |
| rounds_duration               | Time available for players to finish each rounds (but the first)                                                                                                                                                  | 60            |
| rounds_count                  | Number of rounds in the game                                                                                                                                                                                      | 15            |

## Technical development

_Pre-requesites: you will need to have a recent version of the Ruby language installed (2.6+)_

Install all application dependencies
```
make install
```

Check your code for linting errors (code style & syntax)
```
make lint   # Ruby style
make eslint # JS Frontend style (not enforced for now)
```

Run the application:
```
make run
```

## License

AGPLv3
