# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_05_18_211110) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "uuid-ossp"

  create_table "buyers", id: :uuid, default: -> { "uuid_generate_v4()" }, force: :cascade do |t|
    t.uuid "game_id"
    t.integer "money"
    t.boolean "online", default: false
    t.string "workflow_state"
    t.string "bret_session"
    t.string "bret_admin"
    t.jsonb "bret_results"
    t.string "name", null: false
    t.string "age", null: false
    t.string "sex", null: false
    t.string "profession", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["game_id"], name: "index_buyers_on_game_id"
  end

  create_table "games", id: :uuid, default: -> { "uuid_generate_v4()" }, force: :cascade do |t|
    t.string "name"
    t.string "workflow_state"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "products", id: :uuid, default: -> { "uuid_generate_v4()" }, force: :cascade do |t|
    t.uuid "round_id"
    t.integer "price"
    t.uuid "seller_id"
    t.integer "quality"
    t.integer "method", default: 0, null: false
    t.integer "penalty", default: 0, null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["round_id"], name: "index_products_on_round_id"
    t.index ["seller_id"], name: "index_products_on_seller_id"
  end

  create_table "rounds", id: :uuid, default: -> { "uuid_generate_v4()" }, force: :cascade do |t|
    t.uuid "game_id"
    t.integer "number"
    t.string "workflow_state"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["game_id"], name: "index_rounds_on_game_id"
  end

  create_table "sellers", id: :uuid, default: -> { "uuid_generate_v4()" }, force: :cascade do |t|
    t.uuid "game_id"
    t.integer "money"
    t.integer "certificate"
    t.boolean "online", default: false
    t.string "workflow_state"
    t.string "bret_session"
    t.string "bret_admin"
    t.jsonb "bret_results"
    t.string "name", null: false
    t.string "age", null: false
    t.string "sex", null: false
    t.string "profession", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["game_id"], name: "index_sellers_on_game_id"
  end

  create_table "settings", id: :uuid, default: -> { "uuid_generate_v4()" }, force: :cascade do |t|
    t.string "s_key", null: false
    t.binary "s_value", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["s_key"], name: "index_settings_on_s_key", unique: true
  end

  create_table "transactions", id: :uuid, default: -> { "uuid_generate_v4()" }, force: :cascade do |t|
    t.uuid "round_id"
    t.integer "number"
    t.uuid "product_id"
    t.uuid "buyer_id"
    t.decimal "utility"
    t.integer "method", default: 0, null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["buyer_id"], name: "index_transactions_on_buyer_id"
    t.index ["product_id"], name: "index_transactions_on_product_id"
    t.index ["round_id"], name: "index_transactions_on_round_id"
  end

  add_foreign_key "buyers", "games"
  add_foreign_key "products", "rounds"
  add_foreign_key "products", "sellers"
  add_foreign_key "rounds", "games"
  add_foreign_key "sellers", "games"
  add_foreign_key "transactions", "buyers"
  add_foreign_key "transactions", "products"
  add_foreign_key "transactions", "rounds"
end
