class CreateTransactions < ActiveRecord::Migration[6.0]
  def change
    create_table :transactions, id: false do |t|
      t.primary_key :id, :uuid, default: "uuid_generate_v4()"
      t.references :round, type: :uuid, index: true, foreign_key: true
      t.integer :number
      t.references :product, type: :uuid, index: true, foreign_key: true
      t.references :buyer, type: :uuid, index: true, foreign_key: true
      t.decimal :utility
      t.integer :method, default: 0, null: false

      t.timestamps
    end
  end
end
