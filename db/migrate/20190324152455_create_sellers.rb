class CreateSellers < ActiveRecord::Migration[6.0]
  def change
    create_table :sellers, id: false do |t|
      t.primary_key :id, :uuid, default: "uuid_generate_v4()"
      t.references :game, type: :uuid, index: true, foreign_key: true
      t.integer    :money
      t.integer    :certificate

      # TODO: Polymorphic table with all the following fields (shared with Buyer model)
      t.boolean    :online, default: false
      t.string     :workflow_state
      t.string     :bret_session
      t.string     :bret_admin
      t.jsonb      :bret_results

      t.string     :name, null: false
      t.string     :age, null: false
      t.string     :sex, null: false
      t.string     :profession, null: false

      t.timestamps
    end
  end
end
