class CreateSettings < ActiveRecord::Migration[6.0]
  def change
    create_table :settings, id: false do |t|
      t.primary_key :id, :uuid, default: "uuid_generate_v4()"
      t.string :s_key, null: false
      t.binary :s_value, null: false

      t.timestamps
    end

    add_index :settings, [:s_key], unique: true
  end
end
