class CreateRounds < ActiveRecord::Migration[6.0]
  def change
    create_table :rounds, id: false do |t|
      t.primary_key :id, :uuid, default: "uuid_generate_v4()"
      t.references :game, type: :uuid, index: true, foreign_key: true
      t.integer :number
      t.string :workflow_state

      t.timestamps
    end
  end
end
