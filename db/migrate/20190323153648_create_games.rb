class CreateGames < ActiveRecord::Migration[6.0]
  def connection
    ActiveRecord::Base.establish_connection(Rails.env.to_s.to_sym).connection
  end

  def change
    create_table :games, id: false do |t|
      t.primary_key :id, :uuid, default: "uuid_generate_v4()"
      t.string :name
      t.string :workflow_state

      t.timestamps
    end
  end
end
