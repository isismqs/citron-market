class EnableUuidExtension < ActiveRecord::Migration[6.0]
  def connection
    ActiveRecord::Base.establish_connection("#{Rails.env}_admin".to_sym).connection
  end

  def change
    enable_extension "uuid-ossp" unless extension_enabled?("uuid-ossp")
  rescue => e
    Rails.logger.info("Something went wrong when enabling pgcrypto extension: #{e.inspect}")
  end
end
