class BuyersReportingView < ActiveRecord::Migration[6.0]
  def up
    execute <<~SQL
      create type buyers_method as enum ('custom', 'cheapest', 'brand', 'timeout', 'segment');
      create view buyers_reporting_view as
      select
            g.name as game
          , r.number as round
          , b.name as buyer
          , b.age
          , b.sex
          , b.profession
          , to_number((b.bret_results->0->'bret.Player.payoff')::text, '9999') as bret_payoff
          , to_number(
              coalesce(
                nullif(
                  (b.bret_results->0->'bret.Player.boxes_collected')::text
                , '""'
                )
              , '0'
              )
            , '9999'
            ) as bret_result
          , b.money
          , 'T' || ROW_NUMBER() OVER(Partition BY b.name, g.name, r.number ORDER BY b.name) as tid
          , t.utility
          , p.price
          , p.quality
          , s.name as brand
          , s.certificate as brand_certificate
          , enumlabel as selection_method
      from
            products as p
      left outer join
            transactions as t on t.product_id = p.id
      inner join
            sellers as s on p.seller_id = s.id
      inner join
            rounds as r on t.round_id = r.id
      inner join
            buyers as b on b.id = t.buyer_id
      inner join
            games as g on r.game_id = g.id
      inner join
            pg_catalog.pg_enum on enumtypid = 'buyers_method'::regtype and t.method+1 = enumsortorder
      where
            t.product_id = p.id
      group by t.id, r.number, b.name, g.name, b.money, p.price, p.quality, s.certificate, s.name, b.bret_results, enumlabel, b.age, b.sex, b.profession
      order by g.name, b.name, r.number, tid;
    SQL
  end

  def down
    execute <<~SQL
      drop view buyers_reporting_view;
      drop type buyers_method;
    SQL
  end
end
