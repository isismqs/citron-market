class SellersReportingView < ActiveRecord::Migration[6.0]
  def up
    execute <<~SQL
      create type sellers_method as enum ('custom', 'same', 'timeout');
      create view sellers_reporting_view AS
      select
            g.name as game
          , r.number as round
          , s.name as seller
          , s.age
          , s.sex
          , s.profession
          , to_number((s.bret_results->0->'bret.Player.payoff')::text, '9999') as bret_payoff
          , to_number(
              coalesce(
                nullif(
                  (s.bret_results->0->'bret.Player.boxes_collected')::text
                , '""'
                )
              , '0'
              )
            , '9999'
            ) as bret_result
          , s.certificate
          , 'P' || ROW_NUMBER() OVER(Partition BY s.name, g.name, r.number ORDER BY s.name) as pid
          , p.quality
          , (p.quality * 4 + 8) as cost
          , p.price
          , price - (p.quality * 4 + 8) as profit
          , p.penalty
          , count(t.utility) as sold
          , enumlabel as selection_method
      from
            products as p
      inner join
            rounds as r on p.round_id = r.id
      inner join
            sellers as s on s.id = p.seller_id
      inner join
            games as g on r.game_id = g.id
      inner join
            pg_catalog.pg_enum on enumtypid = 'sellers_method'::regtype and p.method+1 = enumsortorder
      left outer join
            transactions as t on p.id = t.product_id
      group by p.id, r.number, s.name, s.certificate, g.name, enumlabel, s.bret_results, s.age, s.sex, s.profession
      order by g.name, s.name, r.number, pid;
    SQL
  end

  def down
    execute <<~SQL
      drop view sellers_reporting_view;
      drop type sellers_method;
    SQL
  end
end
