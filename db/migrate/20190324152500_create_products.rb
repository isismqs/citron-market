class CreateProducts < ActiveRecord::Migration[6.0]
  def change
    create_table :products, id: false do |t|
      t.primary_key :id, :uuid, default: "uuid_generate_v4()"
      t.references :round, type: :uuid, index: true, foreign_key: true
      t.integer :price
      t.references :seller, type: :uuid, index: true, foreign_key: true
      t.integer :quality
      t.integer :method, default: 0, null: false
      t.integer :penalty, default: 0, null: false

      t.timestamps
    end
  end
end
