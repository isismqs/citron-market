require_relative "boot"

require "rails/all"

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Thesis
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 6.0

    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration can go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded after loading
    # the framework and any gems in your application.

    # ActionCable configuration
    config.action_cable.allowed_request_origins = [ENV.fetch("THESIS_HOST") { "http://localhost:3000" }]
    config.action_cable.mount_path = "/live"
    config.action_cable.worker_pool_size = ENV.fetch("RAILS_MAX_THREADS") { 5 }
  end
end
