Rails.application.routes.draw do
  root "games#index"

  concern :player do
    member do
      get "getting_started"
      get "play"
      get "end"
    end
  end

  concern :seller_api do
    collection do
      post "market_setup"
    end
  end

  concern :buyer_api do
    collection do
      post "purchase"
    end
  end

  concern :game_api do
    collection do
      post "wait"
    end
  end

  resources :games, shallow: true do
    member do
      post "join"
    end

    resources :buyers, concerns: :player
    resources :sellers, concerns: :player
  end

  scope :api do
    defaults format: :json do
      resources :sellers, concerns: :seller_api
      resources :buyers, concerns: :buyer_api
      resources :games, concerns: :game_api
    end
  end

  namespace :admin do
    resources :games do
      member do
        post "refresh"
        get "results", defaults: {format: "csv"}
      end
    end
    resources :players
    resources :settings
  end

  resources :admin

  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
