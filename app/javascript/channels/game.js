import _ from 'lodash';

import consumer from "../channels/consumer";
import { parseJSONApiResponse } from "../utils/jsonApi";

function createGame(reactComponent) {
  return consumer.subscriptions.create("GameChannel", {
    connected() {
      console.log("Connected to the websocket!"); // debug only
    },

    disconnected() {
      console.log("Bye bye websocket."); // debug only
    },

    refreshData() {
      // Calls `GameChannel#refresh(data)` on the server.
      this.perform("refresh", {});
    },

    received(body) {
      const newState = parseJSONApiResponse(body);

      // Reload page if game is changing state
      if (newState.game) {
        if (newState.game.to_state) {
          location.reload();
        }
      }

      reactComponent.setState(newState);
    }
  });
}

export default createGame;
