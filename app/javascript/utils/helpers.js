//////////////////////////////////////////////////////////////////////////
// /!\ This function is shared between the frontend and the backend /!\ //
//////////////////////////////////////////////////////////////////////////
function costFunction (value, max, coef, initial) {
  return initial + (coef / (max-1)) + ((value-1) * coef) * ((value-1) / (max-1));
}

function productCostFunction (value) {
  return 8 + 4 * value;
}

export { costFunction, productCostFunction };
