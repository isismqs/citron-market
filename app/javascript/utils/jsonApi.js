import axios from 'axios';
import _ from 'lodash';

const PURCHASE_CUSTOM = 'custom';
const PURCHASE_CHEAPEST = 'cheapest';
const PURCHASE_BRAND = 'brand';
const PURCHASE_TIMEOUT = 'timeout';
const PURCHASE_SEGMENT = 'segment';

const SELLING_CUSTOM = 'custom';
const SELLING_SAME = 'same';
const SELLING_TIMEOUT = 'timeout';

// ORDER IS IMPORTANT and shared with backend. Don't reorder this list.
const PURCHASE_METHODS = [PURCHASE_CUSTOM, PURCHASE_CHEAPEST, PURCHASE_BRAND, PURCHASE_TIMEOUT, PURCHASE_SEGMENT];
// ORDER IS IMPORTANT and shared with backend. Don't reorder this list.
const SELLING_METHODS  = [SELLING_CUSTOM, SELLING_SAME, SELLING_TIMEOUT];

class API {

  constructor () {
    this.client = axios.create({
      baseURL: '/api'
    });

    this.sellerMarketSetup = this.sellerMarketSetup.bind(this);
  }

  sellerMarketSetup (products, certificate = null, round = null, successCallback = null, errorCallback = null) {
    this.genericPost('/sellers/market_setup', { products, certificate, round }, successCallback, errorCallback);
  }

  buyerPurchase (products, method = PURCHASE_CUSTOM, successCallback = null, errorCallback = null) {
    this.genericPost('/buyers/purchase', { products, method }, successCallback, errorCallback);
  }

  wait (round = null, successCallback = null, errorCallback = null) {
    this.genericPost('/games/wait', { round }, successCallback, errorCallback);
  }

  genericPost (url, data, successCallback = null, errorCallback = null) {
    const success = successCallback || (response => console.log(response));
    const error = errorCallback || (response => console.log(response));

    this.client.post(url, data)
      .then(success)
      .catch(error);
  }
}

const api = new API();

function findObject(allObjects) {
  return (object) => {
    var includedObject = _.find(allObjects, (el) => el.id === object.id && el.type === object.type);

    if (includedObject) {
      return flattenJsonAPIObject(includedObject, allObjects);
    } else {
      return object;
    }
  };
}

function parseJSONApiResponse(body) {
  var newBody = {};
  const objects = _([body.data]).filter(el => {
    return el != null;
  }).value();

  _(objects).forEach(object => {
    newBody[object.type] = flattenJsonAPIObject(object, body.included);
  });

  return newBody;
}

function flattenJsonAPIObject(object, allObjects) {
  return Object.assign(
    {},
    object.attributes,
    _(object.relationships || {}).toPairs().map(([k, v]) => {
      var relObject = v.data;
      var newObject;

      if (Array.isArray(relObject)) {
        newObject = _(relObject).map(findObject(allObjects)).value();
      } else {
        newObject = relObject != null ? findObject(allObjects)(relObject) : null;
      }

      return [k, newObject];
    }).filter(([, v]) => { return v != null; }).fromPairs().value(),
    { id: object.id, type: object.type }
  );
}

export {
  flattenJsonAPIObject,
  parseJSONApiResponse,
  PURCHASE_METHODS,
  SELLING_METHODS,
  api as default
};
