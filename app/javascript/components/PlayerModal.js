import React from 'react';
import PropTypes from 'prop-types';
import { Alert, Badge, Modal, Button, Spinner } from 'react-bootstrap';
import _ from 'lodash';

import api from '../utils/jsonApi';
import _s from '../utils/settings';

class PlayerModal extends React.Component {
  static propTypes = {
    config: PropTypes.object.isRequired,
    player: PropTypes.object.isRequired,
    round: PropTypes.number.isRequired,
    keyboard: PropTypes.bool.isRequired,
    backdrop: PropTypes.string.isRequired,
    size: PropTypes.string.isRequired,
    show: PropTypes.bool.isRequired
  }

  constructor (props) {
    super(props);

    this.state = {
      waiting: false
    };

    this.confirmationHandler = this.confirmationHandler.bind(this);
  }

  renderConfirmButtonContent () {
    const { waiting } = this.state;
    const { round, config } = this.props;

    if (waiting) {
      return (
        <div>
          <Spinner
            as='span'
            animation='border'
            size='sm'
            role='status'
            aria-hidden='true'
            />
          <span className='ml-2'>Waiting for all players...</span>
        </div>
      );
    } else {
      if (round < config[_s.ROUNDS_COUNT]) {
        return <span>Ok, begin next round</span>;
      } else {
        return <span>Ok, end game</span>;
      }
    }
  }

  renderAuditResults () {
    const { player } = this.props;
    const ongoingAudit = !!player.ongoing_audit;
    const penaltyProducts = _.filter(player.products, product => product.penalty > 0);

    if (penaltyProducts.length > 0) {
      return (
        <>
          <b className='mx-2'>−</b>
          <Badge variant='danger'>Audit penalty {player.current_round_penalty} €</Badge>
          <br/>
          <small className="text-danger">
            {`The randomly audited product did not match your brand segment (quality ${penaltyProducts[0].quality} < segment ${player.certificate}).`}
          </small>
        </>
      )
    } else if (ongoingAudit) {
      return (
        <>
          <br/>
          <small className="text-success">
            The randomly audited product matched your brand segment. No fine is applied to your profit.
          </small>
        </>
      )
    } else {
      return null;
    }
  }

  renderPlayerResults () {
    const { player } = this.props;

    let results = [];
    let parts = [];
    let details = [];
    let components = [];

    if (player.type === 'seller') {
      results = [
        `${player.previous_round_profit} €`,
        `${player.current_round_profit} €`
      ];
      parts = [
        'Previous profit',
        'Current profit',
      ];
      details = [
        null,
        (
          <>
            <span>
              <em className='mx-2'>=</em>
              <Badge variant={player.color}>Products profit {player.current_round_products_profit} €</Badge>
              <b className='mx-2'>−</b>
              <Badge variant={player.color}>Certificate payment {player.current_round_certificate_paid} €</Badge>
              {this.renderAuditResults()}
            </span>
          </>
        )
      ];
    } else {
      results = [
        player.previous_round_utility,
        player.current_round_utility
      ];
      parts = [
        'Previous satisfaction',
        'Current satisfaction'
      ];
      details = [
        null,
        null
      ]
    }
    _.each(parts, (part, i) => {
      components.push(
        <div key={i}>
          <h5>{part}: <Badge variant={player.color}>{results[i]}</Badge> {details[i]}</h5>
        </div>
      );
    });

    return components;
  }

  renderAuditWarning () {
    const { player, round, config } = this.props;

    if (round === (config[_s.ROUNDS_START_AUDIT] - 1) && player.type === 'seller') {
      return (
        <Alert variant='danger'>
          <h3>⚠️ Market is being regulated now</h3>
          <p>From now on, your production will be audited.</p>
          <p>One of your items will be randomly selected for inspection. If the quality of the audited item does not match the quality announced by your brand segment (stars), you will not receive the profits for this item and you will <b>pay a fine</b> equal to the price you had set for the nonconformity item.</p>
        </Alert>
      );
    } else if (round === (config[_s.ROUNDS_START_PUBLIC_AUDIT] - 1) && player.type === 'buyer') {
      return (
        <Alert variant='success'>
          <h3>ℹ️️ Customers are more protected now</h3>
          <p>From now on, sellers' production will be audited.</p>
          <p>One of their items will be randomly selected for inspection. If the quality of the audited item does not match the quality announced by their brand segment (stars) they will be <b>punished with a fine</b>.</p>
          <p>Now items' quality is more likely to match their brands' segment.</p>
        </Alert>
      );
    }
  }

  confirmationHandler () {
    const { round } = this.props;

    api.wait(
      round,
      success => {},
      error => {
        this.setState({ waiting: false });
        if (error.response && error.response.status === 410) {
          window.location.reload();
        }
      }
    );

    this.setState({ waiting: true });
  }

  render() {
    const { props } = this;
    const { player } = props;

    return (
      <Modal
        {...props}
        aria-labelledby="contained-modal-title-vcenter"
        centered>
        <Modal.Header>
          <Modal.Title id="contained-modal-title-vcenter">
            Round results
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {this.renderPlayerResults()}
          {this.renderAuditWarning()}
        </Modal.Body>
        <Modal.Footer>
          <Button variant={player.color} onClick={this.confirmationHandler} disabled={this.state.waiting}>
            {this.renderConfirmButtonContent()}
          </Button>
        </Modal.Footer>
      </Modal>
    );
  }
}

export default PlayerModal;
