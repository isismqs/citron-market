import React from 'react';
import PropTypes from 'prop-types';
import { Alert, Button, DropdownButton, Dropdown, OverlayTrigger, Tooltip, Modal } from 'react-bootstrap';

import WaitingButton from "../components/WaitingButton";
import api, { PURCHASE_METHODS } from '../utils/jsonApi';
import _s from '../utils/settings';

import BuyerIconBrand from 'images/buyer-icon-brand.svg'
import BuyerIconCustom from 'images/buyer-icon-custom.svg'
import BuyerIconCheapest from 'images/buyer-icon-cheapest.svg'
import BuyerIconSegment from 'images/buyer-icon-segment.svg'

class ConfirmPurchase extends React.Component {
  static propTypes = {
    game: PropTypes.object.isRequired,
    config: PropTypes.object.isRequired,
    player: PropTypes.object.isRequired,
    selectedProducts: PropTypes.array.isRequired,
    products: PropTypes.array.isRequired
  }

  constructor(props) {
    super(props);

    this.state = {
      alreadyPurchased: this.hasAlreadyPurchased(),
      confirmModal: false
    };

    this.maxProducts = props.config[_s.BUYER_PRODUCTS_COUNT];


    this.purchaseHandler = this.purchaseHandler.bind(this);
    this.forcePurchaseHandler = this.forcePurchaseHandler.bind(this);
    this.cheapestHandler = this.cheapestHandler.bind(this);
    this.satisfactionHandler = this.satisfactionHandler.bind(this);
    this.customHandler = this.customHandler.bind(this);
    this.totalPrice = this.totalPrice.bind(this);
    this.closeConfirmModal = this.closeConfirmModal.bind(this);
    this.choosenProductIds = this.choosenProductIds.bind(this);
  }

  totalPrice () {
    const { products } = this.props;

    return _.sumBy(
      _.filter(products, product => _.includes(this.choosenProductIds(), product.id)),
      'price'
    );
  }

  hasAlreadyPurchased () {
    const { player } = this.props;

    return player.transactions.length > 0;
  }

  choosenProductIds () {
    return _.slice(
      _.map(this.forcedProducts||[], p => p.id),
      0,
      this.maxProducts
    );
  }

  purchaseHandler () {
    api.buyerPurchase(this.choosenProductIds(), this.forcedReason, success => {
      this.setState({ confirmModal: false, alreadyPurchased: true });
    }, error => {
      if (error.response && error.response.status == 409) {
        this.setState({ confirmModal: false, alreadyPurchased: true });
      } else if (error.response) {
        this.setState({ confirmModal: false, error: error.response.data });
      } else {
        this.setState({ confirmModal: false, error: "Empty error response. Please ask for help to the organiser." });
      }
    });
  }

  forcePurchaseHandler () {
    const { products } = this.props;

    this.forcedReason = PURCHASE_METHODS[3];
    this.forcedProducts = products;
    this.purchaseHandler();
  }

  customHandler () {
    const { selectedProducts } = this.props;

    this.forcedReason = PURCHASE_METHODS[0];
    this.forcedProducts = selectedProducts;
    this.displayConfirmModal();
  }

  segmentHandler (i) {
    const component = this;

    return () => {
      const { products, game } = component.props;
      const sellerIds = _.map(
        _.filter(game.sellers, seller => seller.certificate === i),
        'id'
      );
      const availableProducts = _.filter(
        products,
        product => _.includes(sellerIds, product.seller_id)
      );
      const cheapestProducts = _.orderBy(availableProducts, ['price']);

      component.forcedReason = PURCHASE_METHODS[4];
      component.forcedProducts = cheapestProducts;
      component.displayConfirmModal();
    }
  }

  cheapestHandler () {
    const { products } = this.props;
    const cheapestProducts = _.orderBy(products, ['price']);

    this.forcedReason = PURCHASE_METHODS[1];
    this.forcedProducts = cheapestProducts;
    this.displayConfirmModal();
  }

  satisfactionHandler () {
    const { player, products } = this.props;

    if (!player.previous_round_transactions || player.previous_round_transactions.length == 0) {
      this.setState({ error: "No transactions from previous rounds" });
    } else {
      const bestTransactions = _.orderBy(player.previous_round_transactions, ['utility'], ['desc']);

      this.forcedReason = PURCHASE_METHODS[2];
      this.forcedProducts = _.filter(products, product => {
        return product.seller_id === bestTransactions[0].product.seller_id;
      });

      this.displayConfirmModal();
    }
  }

  componentDidMount () {
    const { game, player } = this.props;
    const { alreadyPurchased } = this.state;

    if (game && !this.timeout && !alreadyPurchased) {
      const inSec = game.current_round[`${player.type}_counter`];

      this.timeout = window.setTimeout(this.forcePurchaseHandler, inSec * 1000);
    }
  }

  componentDidUpdate () {
    const { game } = this.props;

    if (game.is_market_closed && this.timeout) {
      window.clearTimeout(this.timeout);
    }
  }

  componentWillUnmount () {
    if (this.timeout) {
      window.clearTimeout(this.timeout);
    }
  }

  displayConfirmModal () {
    this.setState({ confirmModal: true });
  }

  closeConfirmModal () {
    this.forcedProducts = null;
    this.setState({ confirmModal: false });
  }

  renderError () {
    const { error } = this.state;
    const handleDismiss = () => this.setState({ error: null });

    if (error) {
      return <Alert variant="danger" onClose={handleDismiss} dismissible>{error}</Alert>;
    } else {
      return null;
    }
  }

  renderDropdownButtons(buttons) {
    const { player } = this.props;

    return _.map(buttons, (button, i) => {
      return (
        <Dropdown.Item
          key={i}
          as="button"
          variant={`outline-${player.color}`}
          onClick={button.handler}
          disabled={button.disabled}
          >
          {button.child}
        </Dropdown.Item>
      );
    });
  }

  renderButtonIcon(img, text) {
    return (
      <span className="buyer-icon">
        <img src={img} /> <br/>
        <small className='text-body'>{text}</small>
      </span>
    );
  }
  renderButton(button, i) {
    const { player } = this.props;

    if (_.isArray(button)) {
      const first = button[0];

      return (
        <div className={`col-md-${first.colSize}`} key={i}>
          <OverlayTrigger
            placement='bottom'
            overlay=<Tooltip>{first.tooltip}</Tooltip> >
            <DropdownButton
              drop='right'
              bsPrefix='btn btn-block btn'
              className="mb-2 mx-auto"
              variant={`outline-secondary`}
              title={first.dropdownChild}
              >
              {this.renderDropdownButtons(button)}
            </DropdownButton>
          </OverlayTrigger>
        </div>
      )
    } else {
      return (
        <div className={`col-md-${button.colSize}`} key={i}>
          <OverlayTrigger
            placement='bottom'
            overlay=<Tooltip>{button.disabled ? 'Not available right now' : button.tooltip}</Tooltip> >
            <Button
              className='mb-2 mx-auto btn-block'
              variant={`outline-secondary`}
              onClick={button.handler}
              disabled={button.disabled}
              >
              {button.child}
            </Button>
          </OverlayTrigger>
        </div>
      );
    }
  }

  render () {
    const { game, player, config } = this.props;
    const { alreadyPurchased } = this.state;

    const total = this.totalPrice();

    if (alreadyPurchased) {
      return (
        <div>
          <p>Your purchases are already done.</p>
          <WaitingButton variant={player.color}>
            Waiting for all buyers to do the same
          </WaitingButton>
        </div>
      );
    } else {
      var buttons = [
        {
          tooltip: "Select 3 products with the lowest prices.",
          child: this.renderButtonIcon(BuyerIconCheapest, "Lowest prices"),
          handler: this.cheapestHandler,
          colSize: 3
        }, {
          tooltip: "Select 3 products from the brand that gave you the most satisfaction in the last round.",
          child: this.renderButtonIcon(BuyerIconBrand, "Last best brand"),
          handler: this.satisfactionHandler,
          disabled: !player.previous_round_transactions || player.previous_round_transactions.length === 0,
          colSize: 3
        },
        _.times(config[_s.SELLER_CERTIFICATE_MAX] - config[_s.SELLER_CERTIFICATE_MIN] + 1, (i) => {
          const cert = i + config[_s.SELLER_CERTIFICATE_MIN];

          return  {
            tooltip: "Select 3 products from the same segment (stars)",
            dropdownChild: this.renderButtonIcon(BuyerIconSegment, "Brand segment"),
            child: `Brand segment (${'⭐'.repeat(cert)})`,
            handler: this.segmentHandler(cert),
            disabled: _.filter(game.sellers, seller => seller.certificate === cert).length === 0,
            colSize: 3
          }
        }),
        {
          tooltip: "Custom selection in the table above. From zero to 3 products.",
          child: this.renderButtonIcon(BuyerIconCustom, "Custom"),
          handler: this.customHandler,
          colSize: 3
        }
      ]

      return (
        <div>
          <div className='row'>
            <div className='col'>
              {this.renderError()}
            </div>
          </div>
          <div className='row'>
            <div className='col'>
              <hr/>
              <h4>Choose how to select your products to buy</h4>
            </div>
          </div>
          <div className='row'>
            {_.map(buttons, (button, i) => this.renderButton(button, i))}
          </div>

          <Modal show={this.state.confirmModal} onHide={this.closeConfirmModal}>
            <Modal.Header closeButton>
              <Modal.Title>Are you sure?</Modal.Title>
            </Modal.Header>
            <Modal.Body>You will buy <b>{this.choosenProductIds().length}</b> products for a total price of <b>{total} €</b></Modal.Body>
            <Modal.Footer>
              <Button variant="outline-secondary" onClick={this.closeConfirmModal}>
                Cancel
              </Button>
              <Button variant={player.color} onClick={this.purchaseHandler}>
                Buy
              </Button>
            </Modal.Footer>
          </Modal>
        </div>
      );
    }
  }
}

export default ConfirmPurchase;
