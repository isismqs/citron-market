import React from 'react';

import WaitingButton from "../components/WaitingButton";
import createGameChannel from "../channels/game";
import { parseJSONApiResponse } from "../utils/jsonApi";

class Live extends React.Component {
  constructor (props) {
    super(props);

    this.createChannel = this.createChannel.bind(this);

    this.channel = null;

    this.state = {
      game: _(parseJSONApiResponse(props.game)).values().head()
    };
  }

  componentDidMount () {
    this.channel = this.createChannel();
  }

  createChannel () {
    createGameChannel(this);
  }

  renderWaiting () {
    const { game } = this.state;

    if (game) {
      if (game.current_state === 'new') {
        return (
            <WaitingButton variant='primary'>
            Please wait for all players to join...
            </WaitingButton>
        );
      } else if (game.current_state === 'finished') {
        return null;
      } else {
        location.reload();
        return null;
      }
    } else {
      return null;
    }
  }

  render () {
    const { game } = this.state;

    if (game) {
      return (
        <div>
          <p>
            <em>There are currently {game.buyers.length} Buyers and {game.sellers.length} Sellers online.</em>
          </p>
          {this.renderWaiting()}
        </div>
      );
    } else {
      return null;
    }
  }
}

export default Live;
