import React from 'react';
import PropTypes from 'prop-types';

class CountdownTimer extends React.Component {
  static propTypes = {
    game: PropTypes.object.isRequired,
    total: PropTypes.number.isRequired,
    reverse: PropTypes.bool,
    className: PropTypes.string,
    startBlink: PropTypes.number,
    blinkClassNames: PropTypes.array
  }

  constructor (props) {
    super(props);

    this.state = {
      counter: this.props.reverse ? 0 : props.total
    }

    const component = this;
    const incr = this.props.reverse ? 1 : -1;

    if (!this.shouldStopInterval()) {
      this.interval = setInterval(
        () => component.setState({ counter: this.state.counter + incr }),
        1000
      );
    }
  }

  lpad (str, padString, length) {
    while (str.length < length)
      str = padString + str;
    return str;
  }

  shouldStopInterval () {
    const { reverse, total } = this.props;

    return reverse && this.state.counter >= total ||
      this.state.counter <= 0;
  }

  componentDidUpdate () {
    const { game } = this.props;

    if (this.shouldStopInterval()) {
      window.clearInterval(this.interval);
    }

    if (game && game.is_market_closed && this.timeout) {
      window.clearInterval(this.interval);
    }

  }

  componentWillUnmount () {
    if (this.interval) {
      window.clearInterval(this.interval);
    }
  }

  render () {
    const component = this;
    const min = Math.floor(this.state.counter / 60);
    const sec = this.state.counter % 60;
    const className = this.props.className || '';
    const blink = this.props.blinkClassNames || ['', ''];
    const startBlink = this.props.startBlink || 0;
    var extraClass = '';

    if (min === 0 && sec <= startBlink) {
      extraClass = blink[(sec + 1) % 2];
    } else {
      extraClass = blink[0];
    }

    return (
      <div className={`${className} ${extraClass}`}>
        {this.lpad(min.toString(), "0", 2)}:{this.lpad(sec.toString(), "0", 2)}
      </div>
    );
  }
}

export default CountdownTimer;
