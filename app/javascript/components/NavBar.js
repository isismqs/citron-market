import React from 'react';

import CountdownTimer from '../components/CountdownTimer'
import _s from '../utils/settings';

class NavBar extends React.Component {
  constructor (props) {
    super(props);
  }

  renderTimer () {
    const { game, player, config } = this.props;
    const displayCounter =
          player.type === 'seller' && !game.is_market_setup ||
          player.type === 'buyer' && game.is_market_setup

    if (displayCounter) {
      return (
        <CountdownTimer
          game={game}
          className={`counter ml-3 badge`}
          blinkClassNames={[`badge-${player.color}`, 'badge-light']}
          startBlink={15}
          total={game.current_round[`${player.type}_counter`]}
          reverse={false} />
      );
    } else {
      return null;
    }
  }

  renderRound () {
    const { game, player, config } = this.props;

    if (game && game.current_round) {
      return (
        <a className='nav-link'>
          Round {game.current_round.number}/{config[_s.ROUNDS_COUNT]}
          {this.renderTimer()}
        </a>
      );
    } else {
      return null;
    }
  }

  render () {
    const component = this;
    const { player } = component.props;

    return (
      <nav className={'navbar navbar-dark fixed-top flex-md-nowrap shadow bg-' + player.color}>
        <div className='container'>
          <a href='#' className='navbar-brand ml-3 mr-0'>{player.name}</a>
          <div className='navbar-nav m-auto'>
            <div className='nav-item active'>
              {component.renderRound()}
            </div>
          </div>
          <span className='navbar-nav navbar-brand px-2'>
            <b>
              {player.type === 'seller' ? 'Brand ' : 'Buyer '}
              {player.position_in_game+1}
            </b>
          </span>
        </div>
      </nav>
    );
  }
}

export default NavBar;
