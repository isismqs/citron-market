import React from 'react';
import { Alert, Badge } from 'react-bootstrap';
import _ from 'lodash';

import BuyerProduct from '../components/BuyerProduct';
import PlayerModal from '../components/PlayerModal';
import ConfirmPurchase from '../components/ConfirmPurchase';
import WaitingButton from "../components/WaitingButton";
import _s from '../utils/settings';

class BuyerTable extends React.Component {
  constructor (props) {
    super(props);

    this.state = {
      selectedProducts: this.initialProducts(),
      shuffledProducts: this.initialShuffledProducts(),
      error: null
    };

    this.allProducts = this.allProducts.bind(this);
    this.isSelected = this.isSelected.bind(this);
    this.toggleSelectProduct = this.toggleSelectProduct.bind(this);
  }

  initialShuffledProducts () {
    const retrievedProducts = _.shuffle(this.allProducts());
    if (retrievedProducts.length > 0) {
      return retrievedProducts;
    } else {
      return [];
    }
  }

  initialProducts () {
    const { config, player } = this.props;
    const transactions = player.transactions;

    return _.map(transactions || [], transaction => {
      return transaction.product;
    });
  }

  allProducts () {
    const { game } = this.props;

    return (game
            && game.sellers.map(seller => {
              return (seller.products || []);
            }).flat()
            || []);
  }

  hasAlreadyPurchased () {
    const { player } = this.props;

    return player.transactions.length > 0;
  }

  isSelected(productId) {
    const present = _.find(this.state.selectedProducts, product => {
      return product.id === productId;
    });

    return !!present;
  }

  toggleSelectProduct (event, childComponent) {
    const { config } = this.props;
    const { shuffledProducts } = this.state;
    const newProductId = event.currentTarget.getAttribute('index');

    if (this.hasAlreadyPurchased()) {
      return this.setState({ error: null });
    } else {
      if (this.isSelected(newProductId)) {
        return this.setState({
          selectedProducts: _.reject(this.state.selectedProducts, product => product.id === newProductId),
          error: null
        });
      } else {
        var maxProducts = config[_s.BUYER_PRODUCTS_COUNT];
        if (this.state.selectedProducts.length < maxProducts){
          this.state.selectedProducts.push(
            _.find(shuffledProducts, product => product.id === newProductId)
          );
          return this.setState({ ...this.state, error: null });
        } else {
          return this.setState({ error: `You can only buy ${maxProducts} products per round. Try unselecting another product if you really want to buy this one 🙂` });
        }
      }
    }
  }

  renderTransaction(transaction) {
    const { game } = this.props;
    const product = transaction.product;
    const seller = _.find(game.sellers, seller => seller.id === product.seller_id);

    return (
      <div className='col-4 col-md-12 col-lg-4 product' key={product.id}>
        <BuyerProduct
          short={true}
          container='div'
          seller={seller}
          product={product}
          />
        <h5 className='bg-light'><Badge variant='light'>{transaction.utility} pts</Badge></h5>
      </div>
    );
  }

  renderProduct(product) {
    const { game } = this.props;
    const seller = _.find(game.sellers, seller => seller.id === product.seller_id);
    const { selectedProducts } = this.state;

    return (
      <BuyerProduct
        key={product.id}
        container='td'
        className='product'
        seller={seller}
        product={product}
        toggleHandler={this.toggleSelectProduct}
        selectedProducts={selectedProducts} />
    );
  }

  renderRowProducts (products, index) {
    const component = this;

    return (
      <tr key={index}>
        {(products || []).map(product => { return component.renderProduct(product); })}
      </tr>
    );
  }

  renderProducts() {
    const { game } = this.props;
    const { shuffledProducts, selectedProducts } = this.state;
    const rowCount = game.sellers.length;
    const columnCount = shuffledProducts.length / rowCount;
    const component = this;

    return (
      <table className='table table-borderless' key={selectedProducts}>
        <tbody>
          {_.times(rowCount, i => {
            return component.renderRowProducts(shuffledProducts.slice(i*columnCount, (i+1)*columnCount), i);
          })}
        </tbody>
      </table>
    );
  }

  componentDidUpdate() {
    const { shuffledProducts } = this.state;

    // It is not advised to set state here but in our case the products
    // are not available at first rendering. The setState is thus wrapped
    // in conditions that avoid infinite loops!
    if (shuffledProducts.length === 0) {
      const retrievedProducts = _.shuffle(this.allProducts());
      if (retrievedProducts.length > 0) {
        this.setState({ shuffledProducts:  retrievedProducts });
      }
    }
  }

  renderError () {
    const { error } = this.state;
    const handleDismiss = () => this.setState({ error: null });

    if (error) {
      return <Alert variant="danger" onClose={handleDismiss} dismissible>{error}</Alert>;
    } else {
      return null;
    }
  }

  renderConfirmButtons () {
    const { game, player, config } = this.props;
    const { selectedProducts, shuffledProducts } = this.state;
    const component = this;

    return (
      <ConfirmPurchase
        game={game}
        player={player}
        config={config}
        selectedProducts={selectedProducts}
        products={shuffledProducts}
        />
    );
  }

  renderTable () {
    const component = this;

    return (
      <div className='col-md-8'>
        {component.renderProducts()}
        {component.renderError()}
        {component.renderConfirmButtons()}
      </div>
    );
  }

  renderPreviousTransactions () {
    const { player, game } = this.props;
    const component = this;

    if (game.current_round && player.previous_round_transactions &&
        player.previous_round_transactions.length > 0) {
      return (
        <Alert className='mb-2' variant={player.color}>
          <h5>In <b>round { game.current_round.number - 1}</b> you had the following satisfaction points per purchased items:</h5>
          <div className='row'>
            {_.map(player.previous_round_transactions, (transaction, i) => {
              return component.renderTransaction(transaction);
            })}
          </div>
        </Alert>
      );
    } else {
      return null;
    }
  }

  renderSidebar () {
    const { player, game } = this.props;

    if (game.current_round && game.current_round.number > 1) {
      return (
        <div className='col-md-4'>
          <Alert className='mb-2' variant={player.color}>
            <h4>
              Satisfaction points: {player.utility}
            </h4>
            <h6>
              Previous round points: {player.previous_round_utility}
            </h6>
            <small>
              (Available cash: {player.money} €)
            </small>
          </Alert>
          {this.renderPreviousTransactions()}
        </div>
      );
    } else {
      return null;
    }
  }

  renderEndRoundModal () {
    const { game, config, player } = this.props;

    return (
      <PlayerModal
        player={game.me||player}
        config={config}
        round={game.current_round.number}
        keyboard={false}
        backdrop='static'
        size='lg'
        show={game.is_market_closed}
        />
    );
  }

  // The valuation information is DUPLICATED in the backend code (to do the utility computation)
  renderValuation () {
    const { player } = this.props;
    return (
      <div className='row'>
        <div className='col'>
          <Alert className='mb-2' variant={player.color}>
            Your <b>personnal</b> valuation for each quality-grade is:
            <b className='ml-3'>quality 1</b> = <b>18 €</b> ⋅ <b>quality 2</b> = <b>22 €</b> ⋅ <b>quality 3</b> = <b>28 €</b> ⋅ <b>quality 4</b> = <b>34 €</b>
          </Alert>
        </div>
      </div>
    );
  }

  render () {
    const { game, player, channel } = this.props;
    const { error, shuffledProducts } = this.state;
    const component = this;

    if (game) {
      if (shuffledProducts.length === 0) {
        return (
          <div className='container'>
            {component.renderValuation()}
            <div className='row'>
              <div className='col-md-8'>
                <p>Sellers are actively working on their strategy to offer you products to buy.</p>
                <p>Get ready!</p>
                <WaitingButton variant={player.color}>
                  <b>Please wait</b> for the market to be setup.
                </WaitingButton>
              </div>
              {component.renderSidebar()}
            </div>
          </div>
        );
      } else {
        return (
          <div className='container'>
            {component.renderValuation()}
            <div className='row'>
              {component.renderTable()}
              {component.renderSidebar()}
            </div>
            {component.renderEndRoundModal()}
          </div>
        );
      }
    } else {
      if (channel) {
        channel.refreshData();
      }
      return <span>Please wait.. If you still see this message after 5 seconds, please refresh the page. Or ask for help.</span>;
    }
  }
}

export default BuyerTable;
