import React from 'react';
import PropTypes from 'prop-types';

import Game from '../components/Game';
import BuyerTable from '../components/BuyerTable';

class GameBuyer extends React.Component {
  static propTypes = {
    config: PropTypes.object.isRequired,
    player: PropTypes.object.isRequired
  }

  render () {
    const component = this;

    return (
      <Game  {...component.props}>
        <BuyerTable />
      </Game>
    );
  }
}

export default GameBuyer;
