import React from 'react';
import _ from 'lodash';
import { Alert, Button, ProgressBar, OverlayTrigger, Tooltip } from 'react-bootstrap';

import WaitingButton from "../components/WaitingButton";
import PlayerModal from '../components/PlayerModal';
import SellerProduct from '../components/SellerProduct';
import api, { SELLING_METHODS } from '../utils/jsonApi';
import { costFunction } from '../utils/helpers';
import _s from '../utils/settings';

class SellerTable extends React.Component {
  constructor (props) {
    super(props);

    this.state = {
      error: null,
      marketReady: this.isMarketReady(),
      products: this.initialProducts(),
      selectedCertificate: this.initialCertificate(),
    };

    this.setSimilarPricesHandler = this.setSimilarPricesHandler.bind(this);
    this.certificateSelectionHandler = this.certificateSelectionHandler.bind(this);
    this.productHandler = this.productHandler.bind(this);
    this.submitHandler = this.submitHandler.bind(this);
    this.timeoutSubmitHandler = this.timeoutSubmitHandler.bind(this);
    this.shouldForceQuality = this.shouldForceQuality.bind(this);
  }

  shouldForceQuality (index) {
    const { game, config, player } = this.props;

    return game.current_round.number < config[_s.ROUNDS_START_AUDIT] &&
      index === 0 && ((this.state && this.state.selectedCertificate) || player.certificate);
  }

  initialCertificate () {
    const { player } = this.props;

    return player.certificate;
  }

  initialProducts () {
    const { config, player } = this.props;
    const products = player.products;
    const defaultProduct = { price: config[_s.PRODUCT_PRICE_MIN], quality: config[_s.PRODUCT_QUALITY_MIN] };
    const component = this;

    return (
      new Array(config[_s.SELLER_PRODUCTS_COUNT])
    ).fill(null).map((_v, index) => {
      const forcedQuality = component.shouldForceQuality(index) ? { quality: player.certificate } : {};
      return products[index] || Object.assign({}, defaultProduct, forcedQuality);
    });
  }

  isMarketReady () {
    const { player } = this.props;

    return player.products.length > 0;
  }

  certificateSelectionHandler (cert) {
    return () => {
      const component = this;
      const { products } = this.state;
      const product = Object.assign(
        products[0],
        { quality: cert }
      );

      products[0] = product;

      component.setState({ selectedCertificate: cert, products: products });
    };
  }

  productHandler (index, product) {
    const { products } = this.state;

    products[index] = Object.assign(
      product,
      { method: SELLING_METHODS[0] }
    );

    this.setState({ products: products });
  }

  setSimilarPricesHandler (event = null, cb = null) {
    const { player } = this.props;
    const previousRoundProducts = _.map(this.state.products, (_product, i) => {
      return Object.assign(
        { method: SELLING_METHODS[1] },
        _.pick(player.previous_round_products[i], ['price', 'quality'])
      );
    });

    if (cb) {
      console.log(cb);
      cb(previousRoundProducts);
    } else {
      // Updating the array completely doesn't seem to work
      // as it doesn't update the child SellerProduct components.
      this.setState({ products: [] }, () => {
        this.setState({ products: previousRoundProducts });
      });
    }
  }

  timeoutSubmitHandler () {
    this.setSimilarPricesHandler(null, products => {
      const forcedProducts = _.map(products, product => {
        return Object.assign(
          product,
          { method: SELLING_METHODS[2] }
        );
      });

      this.setState({ products: forcedProducts }, () => {
        this.submitHandler();
      });
    });
  }

  submitHandler () {
    const { game } = this.props;

    api.sellerMarketSetup(
      this.state.products,
      this.state.selectedCertificate,
      game.current_round.number,
      success => {
        this.setState({ marketReady: true });
      },
      error => {
        if (error.response && error.response.status === 409) {
          this.setState({ marketReady: true });
        } else if (error.response && error.response.status === 410) {
          window.location.reload();
        } else if (error.response) {
          this.setState({ error: error.response.data });
        } else {
          this.setState({ error: "Empty error response. Please ask for help to the organiser." });
        }
      }
    );
  }


  renderProduct (index) {
    const { config, player } = this.props;
    const { marketReady, selectedCertificate } = this.state;
    const product = this.state.products[index];

    return (
      <SellerProduct
        config={config}
        player={player}
        key={index}
        index={index}
        product={product}
        marketReady={marketReady}
        productHandler={this.productHandler}
        shouldForceQuality={this.shouldForceQuality}
        selectedCertificate={selectedCertificate} />
    );
  }

  renderCertificateTitle (display) {
    return display ? (
        <h5 className='title'>Brand certificate</h5>
    ) : null;
  }

  // Warning: this is an addition to the initial sessions. The text is in French
  renderCertificateName (i) {
    switch(i) {
    case 1:
      return <h6>Bridge</h6>;
    case 2:
      return <h6>Premium</h6>;
    case 3:
    default:
      return <h6>Haut de gamme</h6>;
    }
  }

  renderCertificate (i) {
    const { selectedCertificate } = this.state;
    const { game, player, config } = this.props;
    const alreadySelected = player.certificate === i;
    const checked = selectedCertificate === i;
    const outline = alreadySelected ? 'alert alert' : 'btn-outline';
    const active  = checked ? ' active' : '';
    const component = this;
    const ratioPaid = (game.current_round.number - 1) / config[_s.SELLER_CERTIFICATE_ROUNDS];
    const percentPaid = ratioPaid < 1 ? Math.floor(ratioPaid * 100) : 100;
    const max = config[_s.SELLER_CERTIFICATE_MAX];
    const coef = config[_s.SELLER_CERTIFICATE_COST_COEF];
    const certificateCost = Math.floor(costFunction(i, max, coef, 20));
    const certificatePaid = (display, percent, color) => {
      if (display) {
        return (
          <span>
            {this.renderCertificateName(i)}
            <ProgressBar animated={percent < 100} variant={color} now={percent} label={`${percent} % paid`} />
          </span>
        );
      } else { return (
          <span>
            {this.renderCertificateName(i)}
            <div>{certificateCost} €</div>
          </span>
      ); }
    };
    const tooltip = <Tooltip>The {certificateCost} € cost will be deducted from your profits from rounds 1 to 4 ({certificateCost/4} € per round)</Tooltip>

    return (
      <OverlayTrigger
        key={i}
        placement='left'
        overlay={tooltip}
        >
        <div key={i} className='mb-2 btn-group-toggle' >
          <label
            className={`certificate-${i} btn ${outline}-${player.color}${active}`}
            onClick={component.certificateSelectionHandler(i)}>
            <input
              type='radio'
              name='certificate'
              id={i}
              defaultChecked={checked}
              autoComplete='off' />
            {component.renderCertificateTitle(alreadySelected)}
            <div className='stars mb-2'>{'⭐'.repeat(i)}</div>
            {certificatePaid(alreadySelected, percentPaid, player.color)}
          </label>
        </div>
      </OverlayTrigger>
    );
  }

  renderBrandCertificate () {
    const { config, player } = this.props;
    const component = this;
    const certificates = player.certificate ? [player.certificate] : _.times(config[_s.SELLER_CERTIFICATE_MAX] - config[_s.SELLER_CERTIFICATE_MIN] + 1, (i) => i + config[_s.SELLER_CERTIFICATE_MIN]);
    const renderCertificates = () => _.map(certificates, i => {
      return component.renderCertificate(i);
    });
    const renderCertificateTitle = () => !player.certificate ? (
        <h5 className='title'>Select a segment for your brand</h5>
    ): null;

    return (
      <div className='certificate mb-2'>
        {renderCertificateTitle()}
        {renderCertificates()}
      </div>
    );
  }

  renderSellerProfit() {
    const { player } = this.props;

    return (
      <div className={'mb-2 alert alert-' + player.color }>
        <h4>
          Total Profit: {player.profit} €
        </h4>
        <h6>
          Previous round profit: {player.previous_round_profit} €
        </h6>
        <small>
          (Available cash: {player.money} €)
        </small>
      </div>
    );
  }

  renderSidebar () {
    const { player } = this.props;
    const component = this;

    return (
      <div className='col-md-4'>
        {component.renderBrandCertificate()}
        {player.certificate ? component.renderSellerProfit() : null}
      </div>
    );
  }

  renderTable () {
    const component = this;
    const { products } = this.state;

    return (
      <div className='col-md-8'>
        <div>
          <table className='table table-borderless table-bordered-head'>
            <thead className='thead-dark'>
              <tr>
                <th>Product</th>
                <th className='table-secondary'>Quality</th>
                <th className='table-secondary'>Cost</th>
                <th className='table-primary'>Price</th>
                <th className='table-secondary'>Profit</th>
              </tr>
            </thead>
            <tbody>
              {_(products).map(function(v, i) {
                return component.renderProduct(i);
              }).value()}
            </tbody>
          </table>
          {component.renderError()}
          {component.renderMarketSubmission()}
        </div>
      </div>
    );
  }

  renderSimilarPriceButton () {
    const { player } = this.props;
    const component = this;

    if (player.previous_round_products.length > 0) {
      return (
        <span className={'float-left btn btn-' + player.color} onClick={component.setSimilarPricesHandler}>
          🔁️ Set same prices & quality as previous round
        </span>
      );
    } else {
      return (null);
    }
  }

  renderError () {
    const { error } = this.state;
    const handleDismiss = () => this.setState({ error: null });

    if (error) {
      return <Alert variant="danger" onClose={handleDismiss} dismissible>{error}</Alert>;
    } else {
      return null;
    }
  }

  renderMarketSubmission () {
    const { game, player } = this.props;
    const { marketReady } = this.state;
    const component = this;

    if (marketReady) {
      var waitingText = 'Waiting for all sellers to do the same.';

      if (game.is_market_setup) {
        waitingText = 'Waiting for buyers to purchase products';
      }
      return (
        <div className='row'>
          <div className='col-sm-6 mb-2'>
            Your market is set.
          </div>
          <div className='col-sm-6 mb-2'>
            <WaitingButton variant={player.color}>
              {waitingText}
            </WaitingButton>
          </div>
        </div>
      );
    } else {
      return (
        <div className='row'>
          <div className='col-sm-6 mb-2'>
            {component.renderSimilarPriceButton()}
          </div>
          <div className='col-sm-6'>
            <Button
              className='mb-2 float-right'
              variant={player.color}
              onClick={component.submitHandler} >
              Confirm market setup
            </Button>
          </div>
        </div>
      );
    }
  }

  renderEndRoundModal () {
    const { game, config, player } = this.props;

    return (
      <PlayerModal
        player={game.me || player}
        config={config}
        round={game.current_round.number}
        keyboard={false}
        backdrop='static'
        size='lg'
        show={game.is_market_closed}
        />
    );
  }

  componentDidMount () {
    const { game, player } = this.props;
    const { marketReady } = this.state;

    if (game && !this.timeout && !marketReady) {
      const inSec = game.current_round[`${player.type}_counter`];

      this.timeout = window.setTimeout(this.timeoutSubmitHandler, inSec * 1000);
    }
  }

  componentDidUpdate () {
    const { game } = this.props;

    if (game && game.is_market_closed && this.timeout) {
      window.clearTimeout(this.timeout);
    }
  }

  componentWillUnmount () {
    if (this.timeout) {
      window.clearTimeout(this.timeout);
    }
  }

  render () {
    const { game, channel } = this.props;
    const component = this;

    if (game && game.current_round) {
      return (
        <div>
          <div className='container'>
            <div className='row'>
              {component.renderTable()}
              {component.renderSidebar()}
            </div>
          </div>
          {component.renderEndRoundModal()}
        </div>
      );
    } else {
      if (channel) {
        channel.refreshData();
      }
      return (
        <div className='container'>
          <div className='row'>
            <span>Please wait.. If you still see this message after 10 seconds, please ask for help.</span>
          </div>
        </div>
      );
    }
  }

}

export default SellerTable;
