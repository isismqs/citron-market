import React from 'react';
import PropTypes from 'prop-types';

import Game from '../components/Game';
import NavBar from '../components/NavBar';

class Nav extends React.Component {
  static propTypes = {
    config: PropTypes.object.isRequired,
    player: PropTypes.object.isRequired
  }

  render () {
    const component = this;

    return (
      <Game {...component.props} >
        <NavBar/>
      </Game>
    );
  }
}

export default Nav;
