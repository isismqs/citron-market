import React from 'react';
import InputRange from 'react-input-range';
import PropTypes from 'prop-types';

import _s from '../utils/settings';
import { productCostFunction } from '../utils/helpers';

class SellerProduct extends React.Component {
  static propTypes = {
    product: PropTypes.object.isRequired,
    config: PropTypes.object.isRequired,
    player: PropTypes.object.isRequired,
    index: PropTypes.number.isRequired,
    marketReady: PropTypes.bool.isRequired,
    productHandler: PropTypes.func.isRequired,
    shouldForceQuality: PropTypes.func.isRequired,
    selectedCertificate: PropTypes.number
  }

  constructor (props) {
    super(props);

    this.state = props.product;

    // Bindings
    this.cost = this.cost.bind(this);
    this.priceChangeHandler = this.priceChangeHandler.bind(this);
    this.qualityChangeHandler = this.qualityChangeHandler.bind(this);
    this.notifyParent = this.notifyParent.bind(this);
  }

  priceChangeHandler (price) {
    this.state.price = price;
    this.notifyParent();
  }

  qualityChangeHandler (quality) {
    this.state.quality = quality;
    this.notifyParent();
  }

  notifyParent () {
    const { index, productHandler } = this.props;

    // Notify the state changes to the parent component
    productHandler(index, this.state);
  }

  cost () {
    const { config } = this.props;
    const { quality } = this.state;
    const max = config[_s.PRODUCT_QUALITY_MAX];
    const coef = config[_s.PRODUCT_QUALITY_COST_COEF];

    return productCostFunction(quality);
  }

  render () {
    const { index, marketReady, config, player, shouldForceQuality, selectedCertificate } = this.props;
    const profit = this.state.price - this.cost();
    const minQuality =  shouldForceQuality(index) ? selectedCertificate||player.certificate : config[_s.PRODUCT_QUALITY_MIN];
    const trClassName = shouldForceQuality(index) ? 'forced-quality' : '';
    const forcedStyle = shouldForceQuality(index) ? {
      width: ((config[_s.PRODUCT_QUALITY_MAX] - minQuality) * 100 /
              (config[_s.PRODUCT_QUALITY_MAX] - config[_s.PRODUCT_QUALITY_MIN])) + '%'
    } : {};
    const inputRangeClassNames = Object.assign(
      {},
      InputRange.defaultProps.classNames,
      {
        activeTrack: 'input-range__track input-range__track--active bg-' + player.color,
        slider: 'input-range__slider bg-' + player.color,
      }
    );

    return (
        <tr className={trClassName}>
        <td className='product'>
          <div className={'losange bg-' + player.color}>
            <div className='product-content'>📦</div>
          </div>
        </td>
        <td className='product-quality'>
          <div className='input-range-wrapper' style={forcedStyle}>
            <InputRange
              disabled={marketReady}
              classNames={inputRangeClassNames}
              maxValue={config[_s.PRODUCT_QUALITY_MAX]}
              minValue={minQuality}
              value={this.state.quality}
              onChange={this.qualityChangeHandler}
              />
          </div>
        </td>
        <td className={`product-cost ${shouldForceQuality(index) ? '' : 'bg-light'} text-secondary`}>{this.cost()} €</td>
        <td className='product-price'>
          <div className='input-group'>
            <InputRange
              disabled={marketReady}
              classNames={inputRangeClassNames}
              maxValue={config[_s.PRODUCT_PRICE_MAX]}
              minValue={config[_s.PRODUCT_PRICE_MIN]}
              value={this.state.price}
              step={5}
              formatLabel={value => `${value} €`}
              onChange={this.priceChangeHandler} />
          </div>
        </td>
        <td className={`product-profit ${shouldForceQuality(index) ? '' : 'bg-light'} text-secondary`}>{profit} €</td>
      </tr>
    );
  }
}

export default SellerProduct;
