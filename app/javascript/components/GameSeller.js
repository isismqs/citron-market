import React from 'react';
import PropTypes from 'prop-types';

import Game from '../components/Game';
import SellerTable from '../components/SellerTable';

class GameSeller extends React.Component {
  static propTypes = {
    config: PropTypes.object.isRequired,
    player: PropTypes.object.isRequired
  }

  render () {
    const component = this;

    return (
      <Game {...component.props}>
        <SellerTable />
      </Game>
    );
  }
}

export default GameSeller;
