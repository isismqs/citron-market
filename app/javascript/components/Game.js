import React from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import createGameChannel from "../channels/game";

import BuyerTable from '../components/BuyerTable';
import SellerTable from '../components/SellerTable';
import NavBar from '../components/NavBar';
import { parseJSONApiResponse } from "../utils/jsonApi";

class Game extends React.Component {
  static propTypes = {
    config: PropTypes.object.isRequired,
    player: PropTypes.object.isRequired,
    game: PropTypes.object.isRequired
  }

  constructor (props) {
    var moreProps = {};
    super(props);

    this.state = {
      player: _(parseJSONApiResponse(props.player)).values().head(),
      game: _(parseJSONApiResponse(props.game)).values().head(),
      channel: createGameChannel(this)
    };
  }

  componentDidMount () {
    if (!this.state.game) {
      this.state.channel.refreshData();
    }
  }

  renderChildren(props) {

    return React.Children.map(props.children, child => {
      if ([BuyerTable, SellerTable].includes(child.type)) {
        return React.cloneElement(child, props)
      } else {
        return child;
      }
    });
  }

  render () {
    const component = this;
    const props = {...component.props, ...component.state };

    return (
      <div>
        <NavBar {...props} />
        <main className='container' role='main'>
          {component.renderChildren(props)}
        </main>
      </div>
    );
  }
}

export default Game;
