import React from 'react';
import PropTypes from 'prop-types';
import { Button, Spinner } from 'react-bootstrap';

class WaitingButton extends React.Component {
  static propTypes = {
    variant: PropTypes.string.isRequired,
  }

  constructor(props) {
    super(props);
  }

  render () {
    const { children, variant } = this.props;

    return (
      <Button className='mb-2' variant={variant} disabled={true}>
        <div>
          <Spinner
            as='span'
            animation='border'
            size='sm'
            role='status'
            aria-hidden='true'
            />
          <span className='ml-2'>
            {children}
          </span>
        </div>
      </Button>
    );
  }
}

export default WaitingButton;
