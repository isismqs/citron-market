import React from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import { Badge } from 'react-bootstrap';
import _ from 'lodash';

class BuyerProduct extends React.Component {
  static propTypes = {
    seller: PropTypes.object.isRequired,
    product: PropTypes.object.isRequired,
    container: PropTypes.oneOfType([PropTypes.object, PropTypes.string]),
    short: PropTypes.bool,
    className: PropTypes.string,
    toggleHandler: PropTypes.func,
    selectedProducts: PropTypes.array
  }

  constructor (props) {
    super(props);

    const { product, toggleHandler, container, className } = props;

    var classNames = []
    if (className) {
      classNames.push(className);
    }
    if (toggleHandler) {
      classNames.push('selectable');
    }
    if (this.isSelected(product.id)) {
      classNames.push('selected');
    }

    this.className = _.join(classNames, ' ');
    this.container = container ? container : 'div';
    this.toggleHandler = toggleHandler ? toggleHandler : () => {};
  }

  isSelected(productId) {
    const present = _.find(this.props.selectedProducts||[], product => {
      return product.id === productId;
    });

    return !!present;
  }
  render () {
    const { seller, product, toggleHandler, short } = this.props
    const sellerCertif = '⭐'.repeat(seller.certificate || 0);
    const brandName = short ? 'B' : 'Brand '
    const component = this;

    return (
      <this.container className={this.className} index={product.id} onClick={this.toggleHandler} >
        <div className={'losange bg-' + seller.color}>
          <div className='product-content'>📦</div>
        </div>
        <p className='mb-2'>{product.price} €</p>
        <h5 className={`bg-${seller.color} mb-0`}>
          <Badge variant={seller.color}>
            {`${brandName}${seller.position_in_game+1}`}
            <sup>{sellerCertif}</sup>
          </Badge>
        </h5>
      </this.container>
    );
  }
}

export default BuyerProduct;
