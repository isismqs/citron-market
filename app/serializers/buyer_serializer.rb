class BuyerSerializer
  include FastJsonapi::ObjectSerializer

  mine = proc { |obj, params|
    params && params[:mine] == obj.id
  }

  has_many :transactions, serializer: TransactionSerializer do |buyer, params|
    if mine.call(buyer, params) || buyer.game.market_closed?
      buyer.current_round_transactions
    else
      []
    end
  end

  attribute :color, :position_in_game

  # Private attributes
  attributes :money,
    :name,
    :id,
    :utility,
    :current_round_utility,
    :previous_round_utility,
    if: mine
  has_many :previous_round_transactions,
    record_type: :transaction,
    serializer: TransactionSerializer,
    if: mine do |buyer, _|
    buyer.game.current_round ?
      buyer.transactions.nth_round(buyer.game.current_round.number - 1) :
      []
  end
end
