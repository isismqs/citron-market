class SellerSerializer
  include FastJsonapi::ObjectSerializer

  mine = proc { |obj, params|
    params && params[:mine] == obj.id
  }

  attribute :certificate
  has_many :products, serializer: ProductSerializer do |seller, params|
    if mine.call(seller, params) || seller.game.market_setup_finished?
      seller.current_round_products
    else
      []
    end
  end

  attributes :color, :position_in_game

  # Private attributes
  attributes :money, :name, :id, if: mine
  attributes :profit,
    :current_round_profit,
    :current_round_certificate_paid,
    :current_round_products_profit,
    :current_round_penalty,
    :ongoing_audit,
    :previous_round_profit,
    if: mine
  has_many :previous_round_products,
    record_type: :product,
    serializer: ProductSerializer,
    if: mine do |seller, _|
    seller.game.current_round ?
      seller.products.nth_round(seller.game.current_round.number - 1) :
      []
  end
end
