class TransactionSerializer
  include FastJsonapi::ObjectSerializer

  has_one :product, serializer: ProductSerializer do |transaction, _|
    transaction.product
  end

  attribute :utility, if: proc { |obj, params|
    params && params[:mine] == obj.buyer.id
  } do |obj|
    obj.utility.round(2)
  end
end
