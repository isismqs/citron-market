class GameSerializer
  include FastJsonapi::ObjectSerializer

  attribute :current_state, &:workflow_state
  has_one   :current_round, record_type: :round, serializer: RoundSerializer do |game, _|
    game.current_round
  end

  attribute :is_market_closed do |obj, _|
    obj.market_closed?
  end

  attribute :is_market_setup do |obj, _|
    obj.market_setup_finished?
  end

  has_many :buyers, serializer: BuyerSerializer
  has_many :sellers, serializer: SellerSerializer

  attribute :to_state, if: proc { |_, params| params.dig(:to_state) } do |_, params|
    params[:to_state]
  end

  has_one :me,
    polymorphic: {Buyer => :buyer, Seller => :seller},
    if: proc { |_, params| params.dig(:player) } do |_, params|
    params[:player]
  end
end
