class RoundSerializer
  include FastJsonapi::ObjectSerializer

  attributes :number, :duration
  attributes :seller_counter, :buyer_counter
end
