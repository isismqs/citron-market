class ProductSerializer
  include FastJsonapi::ObjectSerializer

  attribute :price, :seller_id

  # Private attributes

  attribute :quality, :penalty, if: proc { |obj, params|
    params && params[:mine] == obj.seller.id
  }
end
