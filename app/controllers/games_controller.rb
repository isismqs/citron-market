class GamesController < ApplicationController
  include GameApiConcern

  def index
    @games = Game.with_new_state
  end

  def join
    new_game!
    player_type = case join_commit_params
                  when Seller.to_s
                    @game.sellers
                  when Buyer.to_s
                    @game.buyers
    end
    bret = BretHelper::Bret.new
    bret_urls = {
      bret_session: bret.session_uri,
      bret_admin: bret.admin_uri,
    }
    @player = player_type.create(
      bret_urls.merge(join_player_params[:player] || {})
    )

    if @player.save
      persist_player_cookie(@player)
      redirect_to @player.url_path
    else
      flash[:error] = @player.errors.full_messages
      render status: :bad_request, action: :show
    end
  end

  def show
    new_game!
  end

  private

  def one_params
    params.require(:id)
  end

  def join_commit_params
    params.require(:commit)
  end

  def join_player_params
    params.require(:game).permit(player: [:name, :age, :sex, :profession])
  end

  def new_game!
    @game = Game.with_new_state.find(one_params)
  end
end
