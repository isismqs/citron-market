class SellersController < ApplicationController
  include PlayConcern
  include SellerApiConcern

  private

  def player_type
    Seller
  end
end
