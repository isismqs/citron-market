class AdminController < ApplicationController
  skip_before_action :check_in_game
  http_basic_authenticate_with name: ENV["THESIS_ADMIN_AUTH_USERNAME"], password: ENV["THESIS_ADMIN_AUTH_PASSWORD"]

  def index
    @games = Game.unscoped.all
  end
end
