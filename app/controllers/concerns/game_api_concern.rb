module GameApiConcern
  extend ActiveSupport::Concern
  include ApiConcern

  included do
    # Don't verify CSRF token (used by html forms) on API endpoints
    # The authentication of the player is already done by the PlayConcern
    skip_before_action :check_in_game, :verify_authenticity_token, only: [:wait]
    before_action :logged_in_player?, :verify_authenticity, only: [:wait]

    def wait
      if @player.current_round.number != round_params
        raise ApiConcern::GoneBadRequest.new
      end
      if @player.waiting?
        render(status: :conflict, plain: "you are already waiting!") && return
      end

      @player.game.with_lock do
        @player.wait!

        if @player.game.are_all_players_waiting?
          @player.game.new_round!
        end
      end

      render json: {}
    end

    private

    def round_params
      params.require(:round).to_i
    end
  end
end
