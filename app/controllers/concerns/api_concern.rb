module ApiConcern
  extend ActiveSupport::Concern

  class CustomBadRequest < RuntimeError
    attr_accessor :message

    def initialize(message: nil)
      self.message = message
    end
  end

  class GoneBadRequest < RuntimeError
  end

  included do
    rescue_from(ApiConcern::CustomBadRequest) do |e|
      render(status: :bad_request, plain: e.message)
    end

    rescue_from(ApiConcern::GoneBadRequest) do |e|
      render(status: :gone, plain: "Try to refresh your page please")
    end

    def verify_authenticity
      unless @player.present?
        render status: :unauthorized, plain: "Are you a player?"
      end
    end
  end
end
