module PlayConcern
  extend ActiveSupport::Concern

  included do
    before_action :logged_in_player?, :game_state_rerouting
    skip_before_action :check_in_game
    layout "game"

    def getting_started
    end

    def play
    end

    def end
    end

    private

    def game_state_rerouting
      current_action = params[:action]
      current_player_id = player_params

      expected_action = @player&.game&.current_action
      if expected_action.present?
        rerouting_needed =
          expected_action != current_action ||
          @player.id != current_player_id
        redirect_to action: expected_action, id: @player.id if rerouting_needed
      else
        forget_player_cookie
        redirect_to root_path
      end
    end

    def player_type
      raise "This is a generic concern. You need to define #{__method__} method in your controller."
    end

    def player_params
      params.require(:id)
    end
  end
end
