module BuyerApiConcern
  extend ActiveSupport::Concern
  include ApiConcern

  included do
    # Don't verify CSRF token (used by html forms) on API endpoints
    # The authentication of the player is already done by the PlayConcern
    skip_before_action :verify_authenticity_token, only: [:purchase]
    skip_before_action :game_state_rerouting, only: [:purchase]
    before_action :verify_authenticity, only: [:purchase]

    def purchase
      return if purchasing_guards

      total_price = @products.sum(:price)
      transactions = []

      ActiveRecord::Base.transaction do
        @player.with_lock do
          unless @player.current_round_transactions.present?
            transactions = products_params.map { |product|
              @player.transactions.create(
                transactions_params.merge(
                  product_id: product,
                  round: @player.current_round
                )
              )
            }
            @player.money -= total_price
            @player.save
          end
        end
      end

      if transactions.present? && transactions.all?(&:valid?)
        @player.valid_market_closing!
        render json: @player.current_round_transactions.map(&:serialize)
      else
        render status: :unprocessable_entity,
               json: transactions.map(&:errors).map(&:full_messages).reject(&:empty?)
      end
    end

    private

    def purchasing_guards
      @products = Product.current_round.where(id: products_params)
      max_to_buy = Setting.find_by_key(Setting::CONFIGURABLE_NUMBER_PRODUCTS_PER_BUYER)
      min_to_buy = Setting.find_by_key(Setting::CONFIGURABLE_NUMBER_PRODUCTS_PER_BUYER_MIN)

      if @products.count < min_to_buy
        bad_request = "#{@products.count} products are not enough. Please select at least #{min_to_buy} products to buy"
      elsif @products.count > max_to_buy
        bad_request = "Too many products selected (Actual #{@products.count} - Expected maximum #{max_to_buy})"
      end
      if bad_request
        raise ApiConcern::CustomBadRequest.new(message: bad_request)
      end

      if @player.current_round_transactions.present?
        render(status: :conflict, plain: "you already bought enough products!") &&
          (return 1)
      end

      if @player.money < @products.sum(:price)
        raise ApiConcern::CustomBadRequest.new(message: "You don't have enough money to buy those products!")
      end
    end

    def products_params
      params.permit(products: []).dig(:products)
    end

    def transactions_params
      params.permit(:method)
    end
  end
end
