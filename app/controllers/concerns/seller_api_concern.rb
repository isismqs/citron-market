module SellerApiConcern
  extend ActiveSupport::Concern
  include ApiConcern

  included do
    # Don't verify CSRF token (used by html forms) on API endpoints
    # The authentication of the player is already done by the PlayConcern
    skip_before_action :verify_authenticity_token, only: [:market_setup]
    skip_before_action :game_state_rerouting, only: [:market_setup]
    before_action :verify_authenticity, only: [:market_setup]

    def market_setup
      return if market_setup_guards

      if @player.game.first_round?
        @player.update(certificate: certificate_params)
      end

      products = []

      ActiveRecord::Base.transaction do
        @player.with_lock do
          unless @player.current_round_products.present?
            products = products_params.map { |product|
              @player.products.create(
                product.merge(round: @player.current_round)
              )
            }
          end
        end
      end

      if products.present? && products.all?(&:valid?)
        @player.current_round_products.reload
        @player.valid_market_setup!
        render json: @player.current_round_products.map(&:serialize)
      else
        render status: :unprocessable_entity,
               json: products.map(&:errors).map(&:full_messages).reject(&:empty?)
      end
    end

    private

    def market_setup_guards
      if @player.current_round.number != round_params
        raise ApiConcern::GoneBadRequest.new
      end

      if @player.current_round_products.present?
        render(status: :conflict, plain: "you already setup your market!") &&
          (return 1)
      end

      count_to_buy = Setting.find_by_key(Setting::CONFIGURABLE_NUMBER_PRODUCTS_PER_SELLER)

      if products_params.count < count_to_buy
        bad_request = "Not enough products in your market setup (Actual #{products_params.count} - Expected #{count_to_buy})"
      elsif products_params.count > count_to_buy
        bad_request = "Too many products in your market setup (Actual #{products_params.count} - Expected #{count_to_buy})"
      end
      if bad_request
        raise ApiConcern::CustomBadRequest.new(message: bad_request)
      end

      if @player.game.first_round?
        begin
          min_cert = Setting.find_by_key(Setting::CONFIGURABLE_SELLER_CERTIFICATE_MIN)
          max_cert = Setting.find_by_key(Setting::CONFIGURABLE_SELLER_CERTIFICATE_MAX)
          unless (min_cert..max_cert).cover?(certificate_params)
            raise ApiConcern::CustomBadRequest.new(message: "This certificate is invalid (Actual #{certificate_params} - Expected to be between #{min_cert}..#{max_cert})")
          end
        rescue ActionController::ParameterMissing => e
          raise ApiConcern::CustomBadRequest.new(message: e.message)
        end
      end
    end

    def products_params
      params.permit(products: [:price, :quality, :method]).dig(:products)
    end

    def round_params
      params.require(:round).to_i
    end

    def certificate_params
      params.require(:certificate).to_i
    rescue ActionController::ParameterMissing => _e
      raise ApiConcern::CustomBadRequest.new(message: "No certificate selected! Please select a certification level before confirming market setup.")
    end
  end
end
