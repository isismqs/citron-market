class ApplicationController < ActionController::Base
  before_action :check_in_game

  private

  def logged_in_player?
    logged_player = cookies.encrypted[:player]
    @player = if logged_player
      player_type, player_id = logged_player.split(":")
      player_type.constantize.find_by_id(player_id)
    end

    @player.present?
  end

  def check_in_game
    if logged_in_player?
      redirect_to @player.url_path
    else
      forget_player_cookie
    end
  end

  def persist_player_cookie(player)
    cookies.encrypted[:player] = {
      value: "#{player.class}:#{player.id}:#{SecureRandom.uuid}",
      expires: 1.year,
    }
  end

  def forget_player_cookie
    cookies.delete(:player)
  end
end
