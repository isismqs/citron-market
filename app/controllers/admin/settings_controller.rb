module Admin
  class SettingsController < AdminController
    def index
      @settings = Setting.all
      @order = order_params[:order]

      if @order
        @settings = @settings.order(@order.to_h)
      end
    rescue ArgumentError
    end

    def create
      @setting = Setting.new(setting_params)
      @setting.parse_human_value!

      if @setting.save
        flash[:info] = "Your custom setting has been set!"
      else
        flash[:error] = @setting.errors.full_messages.join("\n")
      end

      redirect_to admin_settings_path
    end

    def destroy
      @setting = current_setting

      @setting.destroy
      flash[:alert] = "Your custom setting has been removed. Default value will now be used"

      redirect_to request.referer || admin_settings_path
    end

    private

    def current_setting
      @setting = Setting.find(params[:id])
    end

    def setting_params
      params.require(:setting).permit(:key, :value)
    end

    def order_params
      params.permit(order: [:created_at, :s_key])
    end
  end
end
