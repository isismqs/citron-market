module Admin
  class PlayersController < AdminController
    include AdminHelper

    def destroy
      @player = current_player
      @game = @player.game

      if !@game.finished?
        @player.destroy!
        flash[:alert] = "#{@player.class.to_s.downcase} #{@player.name} has been removed."

        redirect_to admin_game_path(@game)
      else
        render :bad_request, plain: "You can only remove a player before or during a game."
      end
    end

    private

    def current_player_type
      params.require(:type).constantize
    end

    def current_player
      @player = current_player_type.find(params.require(:id))
    end
  end
end
