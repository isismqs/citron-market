module Admin
  class GamesController < AdminController
    include AdminHelper

    def index
      @games = Game.all
      @order = order_params[:order]

      if @order
        @games = if @order[:workflow_state]
          @games.order_by_state(@order[:workflow_state])
        else
          @games.order(@order.to_h)
        end
      end
    rescue ArgumentError
    end

    def new
    end

    def show
      current_game
    end

    def refresh
      current_game.broadcast_status
      redirect_to request.referer || admin_games_path
    end

    def results
      player_type = params.require(:type).to_s.downcase
      unless [Buyer, Seller].map(&:to_s).map(&:downcase).include?(player_type)
        raise "Only 'buyer's or 'seller's results type can be retrieved (passed #{player_type})"
      end

      respond_to do |format|
        format.csv do
          sql_query = "SELECT * FROM #{player_type}s_reporting_view WHERE game = '#{current_game.name}'"
          filename = [
            ActiveSupport::Inflector.transliterate(current_game.name.downcase.gsub(/\s/, "_")),
            player_type,
            "results",
          ].join(".")
          rows = []

          stream_query_rows(sql_query) do |row|
            rows << row
          end

          render csv: rows.join(""), filename: filename
        end
      end
    end

    def create
      @game = Game.new(game_params)

      # <wake-up Heroku>
      BretHelper::Bret.new.fetch(BretHelper::Bret::BRET_DEMO_HOST)
      # </wake-up Heroku>

      @game.save
      flash[:info] = <<~INFO
        Your game has been created!
        You can now share the public url #{random_earth}
        #{request.protocol}#{request.host}:#{request.port}#{@game.public_url}
      INFO
      redirect_to admin_game_path(@game)
    end

    def update
      @game = current_game

      command = update_game_params

      if command.present?
        case command[:event]
        when "start"
          @game.start!
        when "stop"
          @game.force_stop!
        when "round"
          @game.new_round!
        end
      end
    rescue Workflow::NoTransitionAllowed
      flash[:error] = "You can't #{command[:event]} this game. Are there any players yet?"
    ensure
      respond_to do |format|
        format.html { redirect_to admin_game_url(@game) }
        format.json { render json: @game }
      end
    end

    def destroy
      @game = current_game

      @game.remove!
      flash[:alert] = "Your game has been removed."

      redirect_to admin_games_path
    end

    private

    def stream_query_rows(sql_query, options = "WITH CSV HEADER")
      conn = ActiveRecord::Base.connection.raw_connection
      conn.copy_data "COPY (#{sql_query}) TO STDOUT #{options}" do
        while (row = conn.get_copy_data)
          yield row
        end
      end
    end

    def current_game
      @game = Game.find(params[:id])
    end

    def game_params
      params.require(:game).permit(:name)
    end

    def update_game_params
      params.require(:game).permit(:event)
    end

    def order_params
      params.permit(order: [:created_at, :name, :workflow_state])
    end
  end
end
