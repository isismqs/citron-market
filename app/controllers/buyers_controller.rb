class BuyersController < ApplicationController
  include PlayConcern
  include BuyerApiConcern

  private

  def player_type
    Buyer
  end
end
