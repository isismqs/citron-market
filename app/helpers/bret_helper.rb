module BretHelper
  class Bret
    BRET_DEMO_HOST = "https://bret-ecopsy.herokuapp.com"
    BRET_DEMO_URL  = "#{BRET_DEMO_HOST}/demo/bret/"

    attr_accessor :admin_uri, :session_uri

    def initialize(admin_uri = nil, session_uri = nil)
      if admin_uri.nil? && session_uri.nil?
        response, uri = fetch(BRET_DEMO_URL)
        self.admin_uri = build_admin_uri(uri)
        self.session_uri = build_session_uri(response)
      else
        self.admin_uri = admin_uri
        self.session_uri = session_uri
      end
    end

    def build_admin_uri(uri)
      uri.sub("SessionStartLinks", "SessionData")
    end

    def build_session_uri(response)
      md = response.body.match(/(#{BRET_DEMO_HOST}\/join\/[^\/]+\/)/i)
      join_uri = md&.captures&.first

      _, session_uri = fetch(join_uri)

      session_uri
    end

    def fetch_player_data
      uri = URI(admin_uri)

      req = Net::HTTP::Get.new(uri)
      req["Content-Type"] = "application/json"

      response = nil
      Net::HTTP.start(
        uri.hostname,
        uri.port,
        use_ssl: (uri.scheme == "https")
      ) do |http|
        http.request(req) do |res|
          response = JSON.parse(res.read_body)
        end
      end

      response
    end

    def fetch(uri_str, limit = 10)
      # You should choose a better exception.
      raise ArgumentError, "too many HTTP redirects" if limit == 0

      uri = URI.parse(uri_str)
      response = Net::HTTP.get_response(uri)

      case response
      when Net::HTTPSuccess then
        [response, uri_str]
      when Net::HTTPRedirection then
        location = response["location"]
        unless location.start_with?("http")
          uri.path = location
          location = uri.to_s
        end
        fetch(location, limit - 1)
      else
        [response.value, uri_str]
      end
    end
  end
end
