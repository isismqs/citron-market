module ApplicationHelper
  def flash_to_bootstrap_alert(name)
    case name.to_s
    when "error"
      "danger"
    when "alert", "warn"
      "warning"
    else
      name.to_s
    end
  end
end
