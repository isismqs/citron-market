module UtilsHelper
  ########################################################################
  # /!\ This function is shared between the frontend and the backend /!\ #
  ########################################################################
  def self.cost_function(value, max, coef, initial = 0)
    initial + (coef / (max - 1).to_f) +
      ((value - 1) * coef) *
      ((value - 1) / (max - 1).to_f)
  end

  def self.product_cost_function(value)
    8 + 4 * value
  end
end
