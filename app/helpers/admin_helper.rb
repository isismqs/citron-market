module AdminHelper
  def order_link(attribute, order: {})
    classnames = ["pure-button", "btn"]
    arrow = "↑"
    opposite_direction = "DESC"

    if order && order[attribute]
      classnames << "pure-button-primary"
      opposite_direction = order[attribute].downcase.to_s == "desc" ? "ASC" : "DESC"
      arrow = opposite_direction == "ASC" ? "↓" : arrow
    end

    link_to arrow,
      url_for(order: {attribute => opposite_direction}),
      class: classnames.join(" ")
  end

  def nav_item_link(name, path)
    link_to name,
      path,
      class: "pure-menu-link #{current_controller?([path]) ? "active" : ""}"
  end

  def random_earth
    case ((rand * 10).floor % 3)
    when 0
      "🌏"
    when 1
      "🌍"
    else
      "🌎"
    end
  end

  def render_game_actions(game)
    rendering = link_to random_earth, game.public_url,
      title: "Public link to the game",
      class: "button-error pure-button btn"

    rendering += link_to "🔄", refresh_admin_game_url(game),
      method: :post, remote: true,
      title: "Force refresh of all players",
      class: "button-error pure-button btn"

    if game.new?
      event = "start"
      icon  = "▶️"
    elsif game.ongoing?
      event = "stop"
      icon  = "⏹️"

      rendering += link_to(admin_game_url(game),
        method: :patch, remote: true,
        title: "Move to next round",
        class: "pure-button btn",
        data: {
          confirm: "You are about to force the next round. Are you sure?",
          params: "game[event]=round",
        }) { "⏩" }
    end

    if event.present?
      rendering += link_to(admin_game_url(game),
        method: :patch, remote: true,
        title: "#{event.capitalize} the game",
        class: "pure-button btn",
        data: {
          confirm: "You are about to #{event} the game. Are you sure?",
          params: "game[event]=#{event}",
        }) { icon }
    end

    rendering += link_to(admin_game_url(game),
      method: :delete,
      title: "Delete the game",
      class: "button-error pure-button btn",
      data: {
        confirm: "Are you sure you want to delete this game?",
      }) { "❌" }

    rendering
  end

  def render_player_actions(player)
    rendering = unless player.game.finished?
      link_to(admin_player_url(id: player.id, type: player.class),
        method: :delete,
        title: "Delete the game",
        class: "button-error pure-button btn",
        data: {
          confirm: "Are you sure you want to delete this player?",
        }) { "❌" }
    end

    rendering
  end

  private

  def current_controller?(names)
    names.include?("/#{params[:controller]}")
  end
end
