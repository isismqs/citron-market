require "workflow"
require "workflow_activerecord"

class Buyer < ApplicationRecord
  include PlayerConcern
  include NameableConcern

  belongs_to :game, inverse_of: :buyers
  has_many :transactions, inverse_of: :buyer, dependent: :destroy
  has_many :current_round_transactions, -> { current_round }, class_name: "Transaction"
  has_many :rounds, through: :transactions
  has_one  :current_round, class_name: "Round", through: :game

  include WorkflowActiverecord

  workflow do
    state :playing do
      event :play, transitions_to: :playing
      event :wait, transitions_to: :waiting
    end
    state :waiting do
      event :play, transitions_to: :playing
    end
  end

  def initialize(opts)
    super(opts)
    self.money = Setting.find_by_key(Setting::CONFIGURABLE_BUYER_INITIAL_MONEY)
  end

  def me
    BuyerSerializer.new(
      self,
      params: {mine: id},
      include: [
        :transactions,
        :'transactions.product',
        :previous_round_transactions,
        :'previous_round_transactions.product',
      ]
    )
  end

  def utility
    transactions.sum(:utility).round(2)
  end

  def current_round_utility
    if game.current_round
      current_round_transactions.sum(:utility).round(2)
    else
      0
    end
  end

  def previous_round_utility
    if game.current_round
      transactions.nth_round(game.current_round.number - 1).sum(:utility).round(2)
    else
      0
    end
  end

  # Override player_concern
  def color
    "dark"
  end

  def valid_market_closing!
    game.valid_market_closing!
  end

  def to_s
    "#<Buyer:#{id} money=#{money}>"
  end
end
