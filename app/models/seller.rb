require "workflow"
require "workflow_activerecord"

class Seller < ApplicationRecord
  include PlayerConcern
  include NameableConcern

  belongs_to :game, inverse_of: :sellers
  has_many :products, inverse_of: :seller, dependent: :destroy
  has_many :current_round_products, -> { current_round }, class_name: "Product"
  has_many :rounds, through: :products
  has_one  :current_round, class_name: "Round", through: :game

  validates_each :certificate do |record, attr, value|
    min = Setting.find_by_key(Setting::CONFIGURABLE_SELLER_CERTIFICATE_MIN)
    max = Setting.find_by_key(Setting::CONFIGURABLE_SELLER_CERTIFICATE_MAX)

    unless !value || (min..max).cover?(value)
      record.errors.add(attr, "must be between #{min} and #{max}")
    end
  end

  include WorkflowActiverecord

  workflow do
    state :playing do
      event :play, transitions_to: :playing
      event :wait, transitions_to: :waiting
    end
    state :waiting do
      event :play, transitions_to: :playing
    end
  end

  def initialize(opts)
    super(opts)
    self.money = Setting.find_by_key(Setting::CONFIGURABLE_SELLER_INITIAL_MONEY)
  end

  def me
    SellerSerializer.new(
      self,
      params: {mine: id},
      include: [:products, :previous_round_products]
    )
  end

  def ongoing_audit
    current_round && current_round.number >= Setting.find_by_key(Setting::CONFIGURABLE_ROUNDS_START_AUDIT)
  end

  def valid_market_setup!
    if ongoing_audit
      audit!
    end
    game.valid_market_setup!
  end

  # Audit will check at least one product (at random) from current round products
  def audit!
    current_round_products.sample.audit!
  end

  def profit
    (products_profit - certificate_paid).round(2)
  end

  def current_round_profit
    if game.current_round
      (
        current_round_products_profit -
        current_round_certificate_paid -
        current_round_penalty).round(2)
    else
      0
    end
  end

  def current_round_certificate_paid
    certificate_cost_per_round.round(2)
  end

  # Total penalty fine (due to audit) is equal to:
  # the absolute value of the fined products' profit + the fixed penalty for each fined products
  def current_round_penalty
    fined_products = current_round_products.with_penalty
    sold_products = fined_products.joins(:transactions)

    (
      (sold_products.sum(:price) -
      sold_products.sum(&:cost)).abs +
      fined_products.sum(:penalty)
    ).round(2)
  end

  def current_round_products_profit
    related_products = current_round_products.joins(:transactions)

    (related_products.sum(:price) - related_products.sum(&:cost)).round(2)
  end

  def previous_round_profit
    if game.current_round
      related_products = products
        .nth_round(game.current_round.number - 1)
        .without_penalty
        .joins(:transactions)

      fined_products = products
        .nth_round(game.current_round.number - 1)
        .with_penalty

      (
        related_products.sum(:price) -
        related_products.sum(&:cost) -
        certificate_cost_per_round(game.current_round.number - 1) -
        fined_products.sum(:penalty)
      ).round(2)
    else
      0
    end
  end

  def to_s
    "#<Seller:#{id} money=#{money} certificate=#{certificate}>"
  end

  private

  def certificate_paid
    if certificate_cost_per_round.positive?
      rounds = game.current_round.number
      unless game.market_closed?
        rounds -= 1
      end
      certificate_cost_per_round * rounds
    else
      certificate_cost
    end
  end

  def products_profit
    Transaction.unscoped do
      related_products = products.without_penalty.joins(:transactions)
      fined_products = products.with_penalty

      related_products.sum(:price) -
        related_products.sum(&:cost) -
        fined_products.sum(:penalty)
    end
  end

  def certificate_cost
    if certificate
      max  = Setting.find_by_key(Setting::CONFIGURABLE_SELLER_CERTIFICATE_MAX)
      coef = Setting.find_by_key(Setting::CONFIGURABLE_SELLER_CERTIFICATE_COST_COEF)

      UtilsHelper.cost_function(certificate, max, coef, 20)
    else
      0
    end
  end

  def certificate_cost_per_round(round = nil)
    round ||= game&.current_round&.number || (+1.0 / 0.0)
    certificate_rounds = Setting.find_by_key(Setting::CONFIGURABLE_SELLER_CERTIFICATE_ROUNDS).to_f
    if round > 0 && round <= certificate_rounds
      (certificate_cost / certificate_rounds)
    else
      0
    end
  end
end
