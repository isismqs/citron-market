class Setting < ApplicationRecord
  CONFIGURABLE_KEYS = {
    CONFIGURABLE_BUYER_INITIAL_MONEY          = "buyer.initial.money" => 2000,
    CONFIGURABLE_SELLER_INITIAL_MONEY         = "seller.initial.money" => 2000,
    CONFIGURABLE_SELLER_CERTIFICATE_MIN       = "seller.certificate.min" => 1,
    CONFIGURABLE_SELLER_CERTIFICATE_MAX       = "seller.certificate.max" => 3,
    CONFIGURABLE_SELLER_CERTIFICATE_COST_COEF = "seller.certificate.coef" => 40,
    CONFIGURABLE_SELLER_CERTIFICATE_ROUNDS    = "seller.certificate.rounds" => 4,
    CONFIGURABLE_NUMBER_PRODUCTS_PER_SELLER   = "seller.products.count" => 3,
    CONFIGURABLE_NUMBER_PRODUCTS_PER_BUYER    = "buyer.products.count" => 3,
    CONFIGURABLE_NUMBER_PRODUCTS_PER_BUYER_MIN = "buyer.products.minimum.count" => 3,
    CONFIGURABLE_PRODUCT_QUALITY_MIN          = "product.quality.min" => 1,
    CONFIGURABLE_PRODUCT_QUALITY_MAX          = "product.quality.max" => 4,
    CONFIGURABLE_PRODUCT_QUALITY_COST_COEF    = "product.quality.coef" => 6,
    CONFIGURABLE_PRODUCT_PRICE_MIN            = "product.price.min" => 1,
    CONFIGURABLE_PRODUCT_PRICE_MAX            = "product.price.max" => 50,
    CONFIGURABLE_ROUNDS_START_AUDIT           = "rounds.start.audit" => 6,
    CONFIGURABLE_ROUNDS_PUBLIC_AUDIT          = "rounds.start.public.audit" => 11,
    CONFIGURABLE_ROUNDS_FIRST_DURATION        = "rounds.first.duration" => 120,
    CONFIGURABLE_ROUNDS_DURATION              = "rounds.duration" => 60,
    CONFIGURABLE_ROUNDS_COUNT                 = "rounds.count" => 15,
  }.freeze

  PLAYER_SEX_CHOICES = ["Female", "Male", "Other / Prefer not to say"]
  PLAYER_JOB_CHOICES = [
    "Management occupations",
    "Business and financial operations occupations",
    "Computer and mathematical occupations",
    "Architecture and engineering occupations",
    "Life, physical, and social science occupations",
    "Community and social services occupations",
    "Legal occupations",
    "Education, training, and library occupations",
    "Arts, design, entertainment, sports, and media occupations",
    "Healthcare practitioners and technical occupations",
    "Healthcare support occupations",
    "Protective service occupations",
    "Food preparation and serving related occupations",
    "Building and grounds cleaning and maintenance occupations",
    "Personal care and service occupations",
    "Sales and related occupations",
    "Office and administrative support occupations",
    "Farming, fishing, and forestry occupations",
    "Construction and extraction occupations",
    "Installation, maintenance, and repair occupations",
    "Production occupations",
    "Transportation and material moving occupations",
    "Military specific occupations",
  ]

  before_save :clear_cache
  after_destroy :clear_cache

  validates :s_key, uniqueness: true
  validate :s_value, :non_empty_value

  def initialize(attributes = {})
    attr_key = attributes.delete(:key)
    attr_value = attributes.delete(:value)
    super(attributes)

    self.s_key = attr_key if attr_key
    self.value = attr_value if attr_value
  end

  def non_empty_value
    if !!value && value.blank?
      errors.add(:s_value, "can't be empty")
    end
  end

  def value=(val)
    self.s_value = Marshal.dump(val)
  end

  def value
    Marshal.load(s_value)
  end

  def parse_human_value!
    self.value = self.class.parse_human_value(value)
  end

  def clear_cache
    self.class.clear_cache(s_key)
  end

  def self.clear_cache(key)
    Rails.cache.delete_matched("#{name}/single/#{key}")
    Rails.cache.delete_matched("#{name}/all")
  end

  def self.find_by_key(key)
    Rails.cache.fetch("#{name}/single/#{key}") do
      value = find_by_s_key(key)&.value
      value.nil? ? CONFIGURABLE_KEYS[key] : value
    end
  end

  def self.config
    custom = Rails.cache.fetch("#{name}/all") {
      custom = all # Select all settings from database
    }

    CONFIGURABLE_KEYS.map { |k, v|
      [
        k,
        custom.find do |s|
          s.s_key == k
        end&.value || v,
      ]
    }.to_h
  end

  # Parse value as a string and try to guess the type of object entered
  def self.parse_human_value(value)
    if value.start_with?("[") && value.end_with?("]")
      value[1..-2].split(",").map(&:strip).map do |el|
        parse_human_value(el)
      end
    elsif ["true", "false"].include?(value)
      value == "true"
    elsif value.length == value.to_i.to_s.split("").count
      value.to_i
    else
      value
    end
  end
end
