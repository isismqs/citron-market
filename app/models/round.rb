require "workflow"
require "workflow_activerecord"

class Round < ApplicationRecord
  belongs_to :game
  has_many :products
  has_many :sellers, through: :products
  has_many :transactions
  has_many :buyers, through: :transactions

  scope :previous, ->(n, game) { where(game_id: game).where("number < ?", n).order("number DESC") }
  scope :next, ->(n, game) { where(game_id: game).where("number > ?", n).order("number ASC") }

  include WorkflowActiverecord

  workflow do
    state :current do
      event :stop, transitions_to: :finished
    end
    state :finished
  end

  scope :current, -> { with_current_state }

  # Time per round (in seconds)
  def duration
    first_round_duration = Setting.find_by_key(Setting::CONFIGURABLE_ROUNDS_FIRST_DURATION)
    round_duration = Setting.find_by_key(Setting::CONFIGURABLE_ROUNDS_DURATION)

    number == 1 ? first_round_duration : round_duration
  end

  def seller_counter
    counter(0)
  end

  def buyer_counter
    counter(duration)
  end

  def previous
    self.class.previous(number, game_id).first
  end

  def next
    self.class.next(number, game_id).first
  end

  private

  def counter(offset)
    counter = offset + duration - (Time.now - created_at).round

    counter.negative? ? 0 : counter
  end
end
