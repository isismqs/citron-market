require "workflow"
require "workflow_activerecord"

class Game < ApplicationRecord
  has_many :sellers, -> { order(:created_at) }, inverse_of: :game, dependent: :destroy
  has_many :buyers, -> { order(:created_at) }, inverse_of: :game, dependent: :destroy
  has_many :rounds, dependent: :destroy
  has_one :current_round, -> { merge(Round.current) }, class_name: "Round"

  include WorkflowActiverecord

  workflow do
    state :new do
      event :start, transitions_to: :ongoing, if: :ready_to_start?
    end
    state :ongoing do
      event :new_round, transitions_to: :ongoing, if: proc { |g| !g.last_round? }
      event :new_round, transitions_to: :finished, if: :last_round?
      event :force_stop, transitions_to: :finished
    end
    state :finished
    state :removed

    on_transition do |from, to, event, *event_args|
      broadcast_status(to)
    end
  end

  default_scope do
    where.not(remove_state)
  end

  scope :order_by_state, ->(dir = :asc) do
    direction = Arel.sql(dir.downcase.to_s == "desc" ? "DESC" : "ASC")
    order(
      states.map { |state|
        "workflow_state = '#{state}' #{direction}"
      }.join(", ")
    )
  end

  # Get the current controller action on which clients should be connected
  # depending on the current_state (workflow_state)
  def current_action
    self.class.state_to_controller_action(workflow_state)
  end

  def current_round_number
    current_round.number
  end

  def start
    collect_players_bret_results!

    new_round
  end

  def new_round
    # End existing current round
    current_round&.stop!

    # Create a new round
    unless last_round?
      sellers.map(&:play!) && buyers.map(&:play!)
      rounds.create(number: rounds.count + 1)
    end
  end

  def remove!
    update_columns(self.class.remove_state)
  end

  def to_s
    "#<Game:#{id} state=#{workflow_state} #buyers=#{buyers.count} #sellers=#{sellers.count}>"
  end

  def public_url
    Rails.application.routes.url_helpers.game_path(id)
  end

  def players
    {buyers: buyers, sellers: sellers}
  end

  def are_all_players_waiting?
    sellers.all?(&:waiting?) && buyers.all?(&:waiting?)
  end

  def ready_to_start?
    players.values.map(&:count).all?(&:positive?)
  end

  def first_round?
    nth_round?(1)
  end

  def last_round?
    nth_round?(Setting.find_by_key(Setting::CONFIGURABLE_ROUNDS_COUNT))
  end

  def valid_market_setup!
    broadcast_status if market_setup_finished?
  end

  def market_setup_finished?
    sellers.all? do |seller|
      seller.current_round_products.present?
    end
  end

  def valid_market_closing!
    broadcast_status if market_closed?
  end

  def market_closed?
    buyers.all? do |buyer|
      buyer.current_round_transactions.present?
    end
  end

  def broadcast_status(to_state = nil)
    players.values.flatten.each do |player|
      send_status(player, to_state)
    end
  end

  def send_status(player, to_state = nil)
    GameChannel.broadcast_to(
      player,
      serializable_hash(
        to_state: to_state,
        player: player,
        mine: player.id
      )
    )
  end

  def serializable_hash(params)
    serializer(params).serializable_hash
  end

  def serializable_json(params)
    serializer(params).serializable_json
  end

  def to_live
    Hash[players.map { |k, v| [k, v.count] }]
      .merge(current_state: workflow_state)
  end

  private

  def collect_players_bret_results!
    [buyers, sellers].map do |players|
      players.each(&:fetch_bret_results!)
    end
  end

  def nth_round?(n)
    current_round &&
      current_round.number >= n
  end

  def serializer(params)
    GameSerializer.new(
      self,
      {
        params: params,
        include: [:sellers, :'sellers.products', :current_round, :me],
      }
    )
  end

  class << self
    def remove_state
      {workflow_state: :removed}
    end

    def states
      Game.workflow_spec.states.keys
    end

    def state_to_controller_action(workflow_state)
      case workflow_state
      when "new"
        "getting_started"
      when "ongoing"
        "play"
      when "finished"
        "end"
      end
    end
  end
end
