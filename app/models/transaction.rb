class Transaction < ApplicationRecord
  # ORDER IS IMPORTANT. Don't reorder this list.
  enum method: [:custom, :cheapest, :brand, :timeout, :segment]

  belongs_to :round
  belongs_to :product
  belongs_to :buyer

  scope :current_round, -> { joins(:round).merge(Round.current) }
  scope :nth_round, ->(n) { joins(:round).where(rounds: {number: n}) }

  default_scope do
    order(:created_at)
  end

  before_create :compute_utility

  def serializer(params)
    TransactionSerializer.new(
      self,
      {
        params: params,
      }
    )
  end

  def serialize(params = {})
    serializer(params).serializable_hash
  end

  private

  def compute_utility
    self.utility = utility_function
  end

  def utility_function
    max_expectation    =
      Setting.find_by_key(Setting::CONFIGURABLE_SELLER_CERTIFICATE_MAX)
    max_grade          =
      Setting.find_by_key(Setting::CONFIGURABLE_PRODUCT_QUALITY_MAX)
    ratio_grade_expect = (max_grade / max_expectation.to_f)

    expectation        = (product.seller.certificate || 0)
    grade              = product.quality
    difference         = (grade - expectation) * ratio_grade_expect

    max_price          =
      Setting.find_by_key(Setting::CONFIGURABLE_PRODUCT_PRICE_MAX)
    price_satisfaction =
      ((valuation(grade) - product.price) / max_price.to_f) * ratio_grade_expect

    expectation + grade + difference + price_satisfaction
  end

  # This information is DUPLICATED in the frontend code (as an Alert message for buyers)
  def valuation(grade)
    if grade == 1
      18
    elsif grade == 2
      22
    elsif grade == 3
      28
    elsif grade == 4
      34
    end
  end
end
