class Product < ApplicationRecord
  # ORDER IS IMPORTANT. Don't reorder this list.
  enum method: [:custom, :same, :timeout]

  belongs_to :round
  belongs_to :seller
  has_many :transactions

  validates :price, :quality, presence: true

  validates_each :quality do |record, attr, value|
    min = Setting.find_by_key(Setting::CONFIGURABLE_PRODUCT_QUALITY_MIN)
    max = Setting.find_by_key(Setting::CONFIGURABLE_PRODUCT_QUALITY_MAX)

    unless !value || (min..max).cover?(value)
      record.errors.add(attr, "must be between #{min} and #{max}")
    end
  end

  validates_each :price do |record, attr, value|
    min = Setting.find_by_key(Setting::CONFIGURABLE_PRODUCT_PRICE_MIN)
    max = Setting.find_by_key(Setting::CONFIGURABLE_PRODUCT_PRICE_MAX)

    unless !value || (min..max).cover?(value)
      record.errors.add(attr, "must be between #{min} and #{max}")
    end
  end

  scope :current_round, -> { joins(:round).merge(Round.current) }
  scope :nth_round, ->(n) { joins(:round).where(rounds: {number: n}) }
  scope :without_penalty, -> { where(penalty: 0) }
  scope :with_penalty, -> { where.not(penalty: 0) }

  default_scope do
    order(:created_at)
  end

  def audit!
    if quality < seller.certificate
      update(penalty: price)
    end
  end

  def cost
    # max  = Setting.find_by_key(Setting::CONFIGURABLE_PRODUCT_QUALITY_MAX)
    # coef = Setting.find_by_key(Setting::CONFIGURABLE_PRODUCT_QUALITY_COST_COEF)

    UtilsHelper.product_cost_function(quality)
  end

  def serializer(params)
    ProductSerializer.new(self, params: params)
  end

  def serialize(params = {})
    serializer(params).serializable_hash
  end
end
