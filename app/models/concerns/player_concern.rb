module PlayerConcern
  extend ActiveSupport::Concern

  included do
    scope :online, -> { where(online: true) }
    scope :offline, -> { where(online: false) }
  end

  def url_path
    if is_a?(Buyer)
      Rails.application.routes.url_helpers.getting_started_buyer_path(self)
    elsif is_a?(Seller)
      Rails.application.routes.url_helpers.getting_started_seller_path(self)
    end
  end

  def connect!
    update(online: true)
  end

  def disconnect!
    update(online: false)
  end

  def position_in_game
    game.sellers.index(self) || game.buyers.index(self)
  end

  def color
    case position_in_game % 7
    when 0
      "primary"
    when 1
      "success"
    when 2
      "info"
    when 3
      "secondary"
    when 4
      "warning"
    when 5
      "danger"
    when 6
      "dark"
    end
  end

  def fetch_bret_results!
    results = BretHelper::Bret.new(bret_admin).fetch_player_data

    if results.present?
      update(bret_results: results)
    else
      update(bret_results: [{error: "impossible to fetch bret results.. Try to do it manually with the admin URL: #{bret_admin}"}])
    end
  end

  def bret_boxes_collected
    bret = bret_results || [{}]

    bret.first["bret.Player.boxes_collected"]
  end

  def bret_payoff
    bret = bret_results || [{}]

    bret.first["bret.Player.payoff"]
  end
end
