module NameableConcern
  extend ActiveSupport::Concern

  included do
    validates :name, length: {minimum: 3}
    validates :age, :sex, :profession, presence: true
  end
end
