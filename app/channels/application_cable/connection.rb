module ApplicationCable
  class Connection < ActionCable::Connection::Base
    identified_by :current_user

    def connect
      self.current_user = find_verified_user
      logger.add_tags "ActionCable", current_user.name
    end

    private

    def find_verified_user
      logged_player = cookies.encrypted[:player]
      if logged_player
        player_type, player_id = logged_player.split(":")
        player_type.constantize.find(player_id)
      else
        reject_unauthorized_connection
      end
    end
  end
end
