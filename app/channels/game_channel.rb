class GameChannel < ApplicationCable::Channel
  def subscribed
    stream_for current_user
    current_user.connect!

    unless current_user.game.ongoing?
      broadcast_game_status
    end
  end

  def unsubscribed
    current_user.disconnect!

    unless current_user.game.ongoing?
      broadcast_game_status
    end
  end

  def broadcast_game_status
    current_user.game.broadcast_status
  end

  def refresh(_data = {})
    if !current_user.game.ongoing?
      broadcast_game_status
    else
      current_user.game.send_status(current_user)
    end
  end
end
