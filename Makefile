-include env.mk
export
ifeq ($(RAILS_ENV), test)
-include env.test.mk
export
endif

define CLEVER_LOGIN
{
  "token": "$(CLEVER_TOKEN)",
  "secret": "$(CLEVER_SECRET)"
}
endef
export CLEVER_LOGIN

env.mk: .env
	sed 's/"//g ; s/=/:=/' < $< > $@

env.test.mk: .env.test
	sed 's/"//g ; s/=/:=/' < $< > $@

.PHONY: ruby
ruby: .ruby-version
	ruby -v

.PHONY: install
install: ruby purecss bootstrapcss
	gem install bundler --version 2.0.1 --force
	bundle install -j $$(nproc) --path vendor

.PHONY: purecss
purecss:
	./scripts/install.sh purecss

bootstrapcss:
	./scripts/install.sh bootstrap

.PHONY: db
db:
	bundle exec rake db:create
	bundle exec rake db:migrate

.PHONY: db-enable-uuid
db-enable-uuid:
	bundle exec rails generate migration enable_uuid_extension

.PHONY: db-generate-model
db-generate-model:
	bundle exec rails generate model Setting s_key:string s_value:binary
	bundle exec rails generate model Game name:string workflow_state:string
	bundle exec rails generate model Buyer game:references money:integer name:string online:boolean workflow_state:string bret_session:string bret_admin:string bret_results:jsonb age:string sex:string profession:string
	bundle exec rails generate model Seller game:references money:integer certificate:integer name:string online:boolean workflow_state:string bret_session:string bret_admin:string bret_results:jsonb age:string sex:string profession:string
	bundle exec rails generate model Round game:references number:integer workflow_state:string
	bundle exec rails generate model Product round:references price:integer seller:references quality:integer method:integer penalty:integer
	bundle exec rails generate model Transaction round:references number:integer product:references buyer:references utility:decimal method:integer

.PHONY: model-generate-serializers
model-generate-serializers:
	bundle exec rails g serializer Game

.PHONY: generate-cable
generate-cable:
	bundle exec rails generate channel GameChannel

.PHONY: install-react
install-react:
	bundle exec rails webpacker:install:react
	bundle exec rails generate react:install

.PHONY: assets-precompile
assets-precompile:
	bundle exec rake assets:precompile

.PHONY: db-test
db-test:
	RAILS_ENV=test bundle exec rake db:migrate

.PHONY: db-drop
db-drop: confirm
	bundle exec rake db:drop

.PHONY: confirm
confirm:
	@( read -p "Are you sure? [y/N] " sure && case "$$sure" in [yY]) true;; *) false;; esac )

.PHONY: routes
routes:
	bundle exec rails routes
.PHONY: run
run: install db
	bundle exec puma

.PHONY: console
console: install db
	bundle exec rails c

.PHONY: lint
lint: install
	bundle exec standardrb

.PHONY: eslint
eslint: install
	npm run lint

.PHONY: audit
audit: install
	bundle exec bundle-audit check --update --file gems.locked

.PHONY: lint-fix
lint-fix: frontend
	bundle exec standardrb --fix

.PHONY: eslint-fix
eslint-fix: frontend
	npm run lint-fix

.PHONY: frontend
frontend:
	yarn install --check-files
	bundle exec rails webpacker:compile

.PHONY: test
test: install db-test frontend
	RAILS_ENV=test bundle exec rake test

.PHONY: doc
doc: install
	[ "$(shell which apt)" ] && sudo apt install -y graphviz
	MODEL=Game bundle exec rake doc:workflow
	MODEL=Round bundle exec rake doc:workflow

.clever-login:
	echo "$${CLEVER_LOGIN}" > $@

.PHONY: setup-clever
setup-clever: .clever-login
	CONFIGURATION_FILE="$<" clever link "$(CLEVER_APP_ID)"

.PHONY: deploy
deploy: .clever-login setup-clever
	CONFIGURATION_FILE="$<" clever deploy --force

.PHONY: stop-app
stop-app: .clever-login setup-clever
	CONFIGURATION_FILE="$<" clever stop
