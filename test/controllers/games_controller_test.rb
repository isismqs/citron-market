require "test_helper"

class GamesControllerTest < ActionDispatch::IntegrationTest
  test "should join the game for an new game" do
    VCR.use_cassette("bret") do
      post join_game_url(
        games(:new),
        commit: :Buyer,
        game: {player: buyers(:no_game).attributes}
      )
    end
    assert_response :redirect
  end

  test "should not join the game if the player didn't fill in the survey" do
    VCR.use_cassette("bret") do
      post join_game_url(
        games(:new),
        commit: :Buyer,
        game: {player: {name: "empty"}}
      )
    end
    assert_response :bad_request
  end

  test "should not join for an ongoing game" do
    assert_raises(ActiveRecord::RecordNotFound) do
      post join_game_url(games(:ongoing))
    end
  end
end
