require "test_helper"

class SellersControllerTest < ActionDispatch::IntegrationTest
  setup do
    join_a_new_game(Seller)
  end

  test "Describe sellers joining a new Game" do
    assert_response :success

    # If player tries to get to the 'play' screen
    get play_seller_url(@player)
    assert_response :redirect
  end

  test "Describe sellers in a 'ongoing' state Game" do
    start_the_game

    # If player tries to get to the 'getting_started' screen
    get getting_started_seller_url(@player)
    assert_response :redirect

    get play_seller_url(@player)
    assert_response :success

    # Setup your product prices
    params = {
      products: [
        {price: 10, quality: 2},
        {price: 40, quality: 4},
      ],
    }
    # Wrong round number
    params[:round] = 21
    post market_setup_sellers_url, params: params
    assert_response :gone
    # Correct round number
    params[:round] = 1

    # Not enough params to setup
    post market_setup_sellers_url, params: params
    assert_response :bad_request
    assert_match(/not enough products/i, response.body)

    # Correct amount of products
    params[:products] << {price: 30, quality: 3}

    # First round needs a certificate
    post market_setup_sellers_url, params: params
    assert_response :bad_request
    assert_match(/No certificate selected/i, response.body)

    # Not in range certificate
    params[:certificate] = 10
    post market_setup_sellers_url, params: params
    assert_response :bad_request
    assert_match(/certificate is invalid/i, response.body)

    # Not in range certificate
    params[:certificate] = 2
    post market_setup_sellers_url, params: params
    assert_response :success

    # Trying to setup your prices again
    post market_setup_sellers_url, params: params
    assert_response :conflict

    # Buyers buy products
    dummy_market_purchases

    # Next round of the game
    assert_next_round_game do
      # Other sellers already waiting
      @game.sellers.where.not(id: @player.id).map(&:wait!)
      # Buyers already waiting
      @game.buyers.map(&:wait!)

      # Let's wait ourself
      post wait_games_url, params: {round: 0}
      assert_response :gone
      post wait_games_url, params: {round: 1}
      assert_response :success
    end

    # Can setup new products market
    params[:round] = 2
    post market_setup_sellers_url, params: params
    assert_response :success
  end
end
