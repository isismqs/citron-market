require "test_helper"

class BuyersControllerTest < ActionDispatch::IntegrationTest
  setup do
    join_a_new_game(Buyer)
  end

  test "Describe buyers joining a new Game" do
    assert_response :success

    # If player tries to get to the 'play' screen
    get play_buyer_url(@player)
    assert_response :redirect
  end

  test "Describe buyers in a 'ongoing' state Game" do
    start_the_game

    # If player tries to get to the 'getting_started' screen
    get getting_started_buyer_url(@player)
    assert_response :redirect

    get play_buyer_url(@player)
    assert_response :success

    # Setup seller's market setup
    dummy_market_setup

    # Not enough products to setup
    post purchase_buyers_url, params: {
      products: @available_products.map(&:id).sample(1),
    }
    assert_response :bad_request

    post purchase_buyers_url, params: {
      products: @available_products.map(&:id).sample(3),
    }
    assert_response :success

    # Trying to purchase products again
    post purchase_buyers_url, params: {
      products: @available_products.map(&:id).sample(3),
    }
    assert_response :conflict

    # Next round of the game
    assert_next_round_game do
      # Sellers already waiting
      @game.sellers.map(&:wait!)
      # Other buyers already waiting
      @game.buyers.where.not(id: @player.id).map(&:wait!)

      # Let's wait ourself
      post wait_games_url, params: {round: 0}
      assert_response :gone
      post wait_games_url, params: {round: 1}
      assert_response :success
    end

    # Cannot buy products from previous rounds
    post purchase_buyers_url, params: {
      products: @available_products.map(&:id).sample(3),
    }
    assert_response :bad_request

    # Setup seller's market setup
    dummy_market_setup

    # Not enough products to setup
    post purchase_buyers_url, params: {
      products: @available_products.map(&:id).sample(1),
    }
    assert_response :bad_request

    # But if settings allow to by less than max then it succeeds
    Setting.create(key: Setting::CONFIGURABLE_NUMBER_PRODUCTS_PER_BUYER_MIN, value: 1)
    post purchase_buyers_url, params: {
      products: @available_products.map(&:id).sample(1),
    }
    assert_response :success
  end
end
