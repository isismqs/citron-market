require "test_helper"

class BretHelperTest < ActiveSupport::TestCase
  test "Bret#new" do
    VCR.use_cassette("bret") do
      bret = BretHelper::Bret.new
      assert_match(/SessionData/, bret.admin_uri)
      assert_match(/bret\/Instructions\/1/, bret.session_uri)
    end
  end

  test "Bret existing game with results" do
    VCR.use_cassette("bret-qd0hcmn6") do
      bret = BretHelper::Bret.new("https://bret-ecopsy.herokuapp.com/SessionData/qd0hcmn6/")
      results = bret.fetch_player_data

      assert_equal "14", results.first["bret.Player.boxes_collected"]
      assert_equal "14.00", results.first["bret.Player.payoff"]
    end
  end
end
