module PlayHelper
  def join_a_new_game(player_type)
    @game = games(:simulation)

    VCR.use_cassette("bret") do
      post join_game_url(@game), params: {
        commit: player_type,
        game: {player: buyers(:john).attributes},
      }
    end
    follow_redirect!

    getting_started_path =
      %r{\/#{player_type.to_s.pluralize.downcase}\/([a-f0-9\-]+)\/getting_started}
    assert_match getting_started_path, path
    @player = player_type.find(getting_started_path.match(path).captures.first)
  end

  def start_the_game
    # Make sure we have enough players
    @game.buyers << buyers(:buyer1)
    @game.sellers << sellers(:seller1)

    # Online players
    @game.buyers.map(&:connect!)
    @game.sellers.map(&:connect!)

    assert @game.ready_to_start?

    VCR.use_cassette("bret-nbklfmk4") do
      @game.start!
    end
  end

  def dummy_market_setup
    assert @game.ongoing?
    current_round = @game.rounds.merge(Round.current).first

    @available_products = 3.times.flat_map {
      @game.sellers.map do |seller|
        seller.products.create(
          price: 10,
          quality: 2,
          round: current_round
        )
      end
    }

    assert @available_products.all?(&:valid?)
  end

  def dummy_market_purchases
    assert @game.ongoing?
    current_round = @game.rounds.merge(Round.current).first
    products = @game.reload.sellers.flat_map(&:current_round_products).sample(3)

    @transactions = 3.times.flat_map {
      @game.buyers.each_with_index.map do |buyer, i|
        buyer.transactions.create(
          product_id: products[i].id,
          round: current_round
        )
      end
    }

    assert @transactions.all?(&:valid?)
  end

  def assert_next_round_game
    previous_round = @game.reload.current_round

    # Next round of the game
    yield

    current_round = @game.reload.current_round

    assert previous_round != current_round, "expect previous round (#{previous_round.id}) to be different from current round (#{current_round.id})"
  end
end
