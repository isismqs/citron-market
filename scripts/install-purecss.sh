#!/usr/bin/env bash

INSTALL_PATH="vendor/assets/stylesheets/purecss"
TMP_FILE="tmp/purecss.zip"

if [ ! -d "${INSTALL_PATH}" ]
then
    if [ ! -f "${TMP_FILE}" ]
    then
        wget -O "${TMP_FILE}" https://github.com/pure-css/pure-release/archive/v1.0.0.zip
    fi

    mkdir -p "${INSTALL_PATH}"
    unzip -j "${TMP_FILE}" -d "${INSTALL_PATH}" && rm -f "${TMP_FILE}"

    echo "✔️ ️Purecss installed!"
fi
