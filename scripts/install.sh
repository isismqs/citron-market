#!/usr/bin/env bash

LIB="${1}"

case "${LIB}" in
    purecss)
        INSTALL_PATH="vendor/assets/stylesheets/purecss"
        RELEASE_URL="https://github.com/pure-css/pure-release/archive/v1.0.0.zip"
        ;;
    bootstrap)
        INSTALL_PATH="vendor/assets/stylesheets/bootstrap"
        RELEASE_URL="https://github.com/twbs/bootstrap/releases/download/v4.3.1/bootstrap-4.3.1-dist.zip"
        ;;
esac
TMP_FILE="tmp/${LIB}.zip"

if [ -n "${INSTALL_PATH}" ] && [ ! -d "${INSTALL_PATH}" ]
then
    if [ ! -f "${TMP_FILE}" ]
    then
        wget -O "${TMP_FILE}" "${RELEASE_URL}"
    fi

    mkdir -p "${INSTALL_PATH}"
    unzip -j "${TMP_FILE}" -d "${INSTALL_PATH}" && rm -f "${TMP_FILE}"

    echo "✔️ ${LIB} installed!"
fi
